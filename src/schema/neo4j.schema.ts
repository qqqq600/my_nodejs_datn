import { Static, Type } from '@sinclair/typebox'
import { numberSchema } from './common.schema'

export const addToCartSchema = Type.Object({
  variant_id: Type.Number({ minimum: 1 }),
  quantity: Type.Number({ minimum: 1 })
})

export type IAddToCart = Static<typeof addToCartSchema>

export const updateToCartSchema = Type.Object({
  variant_id: Type.Number({ minimum: 1 }),
  quantity: Type.Number({ minimum: 1 })
})

export type IUpdateToCart = Static<typeof updateToCartSchema>

export const deleteFromCartSchema = Type.Object({
  variant_id: numberSchema
})

export type IDeleteFromCart = Static<typeof deleteFromCartSchema>

export const addToWishSchema = Type.Object({
  product_id: Type.Number({ minimum: 1 })
})

export type IAddToWish = Static<typeof addToWishSchema>

export const deleteFromWishSchema = Type.Object({
  product_id: numberSchema
})

export type IDeleteFromWish = Static<typeof deleteFromWishSchema>

export const addViewedProductSchema = Type.Object({
  product_id: Type.Number({ minimum: 1 })
})

export type IAddViewedProduct = Static<typeof addViewedProductSchema>

export const getViewedProductSchema = Type.Object({
  limit: Type.Optional(numberSchema),
  page: Type.Optional(numberSchema)
})

export type IGetViewedProduct = Static<typeof getViewedProductSchema>

export const getRecommendProductFromUserSchema = Type.Object({
  limit: Type.Optional(numberSchema),
  page: Type.Optional(numberSchema),
  root_category_id: Type.Optional(numberSchema)
})

export type IGetRecommendProductFromUser = Static<typeof getRecommendProductFromUserSchema>
