import { CodeKeyType, MESSAGE, CODE, MessageKeyType } from '@/config/constants'

type Fields = {
  [key: string]: any
}

export const resultTemplate = (code: CodeKeyType, message: MessageKeyType, data?: Fields) => ({
  code: CODE[code],
  message: MESSAGE[message],
  data
})
