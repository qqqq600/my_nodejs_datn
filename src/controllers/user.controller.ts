import { STAFF_ROLE } from '@/config/constants'
import { AppError } from '@/config/appError'
import { NextFunction, Request, Response } from 'express'
import { hash, compare } from 'bcryptjs'

import {
  ICreateUser,
  IUserLogin,
  ICreateRecipientInformation,
  IUpdateRecipientInformation,
  IRemoveRecipientInformation,
  IUpdateUserInformation,
  IGetOtpForgotPassword,
  IVerifyOtpCode,
  IResetPasswordForgot,
  IChangePassword
} from '@/schema/user.schema'
import prisma from '@/libs/prisma'
import { resultTemplate } from '@/utils/result-template'
import { createJSONToken } from '@/utils/jwt'
import { session } from '@/neo4j/configDbNeo4j'
import dayjs from 'dayjs'
import { IGetUserDetail } from '@/schema/user.schema'
import { deleteImage } from '@/services/s3.service'
import { getUserInfoGoogle } from '@/services/google.service'
import { resetPwdUserOtpService, resetPwdUserSuccessService } from '@/services/mail.service'

export const getAllUser = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const users = await prisma.user.findMany()
    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', users))
  } catch (error: any) {
    next(error)
  }
}

export const createUser = async (req: Request, res: Response, next: NextFunction) => {
  const { name, email, password, phone_number }: ICreateUser = req.body

  try {
    const existingEmail = await prisma.user.findFirst({
      where: {
        email: email
      }
    })
    if (existingEmail) {
      throw new AppError('BAD_REQUEST', 'EMAIL_EXISTED')
    }
    const existingPhoneNumber = await prisma.user.findFirst({
      where: {
        phone_number: phone_number
      }
    })
    if (existingPhoneNumber) {
      throw new AppError('BAD_REQUEST', 'PHONE_NUMBER_EXISTED')
    }
    const hashedPw = await hash(password, 12)

    const newUser = await prisma.user.create({
      data: {
        name: name || null,
        email: email,
        password: hashedPw,
        phone_number: phone_number
      }
    })
    await session.run('MERGE (u:User {id: $id, email: $email})', { id: newUser.id, email: newUser.email })

    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', newUser))
  } catch (error: any) {
    next(error)
  }
}

export const userLogin = async (req: Request, res: Response, next: NextFunction) => {
  const { email, password }: IUserLogin = req.body
  const now = dayjs()
  try {
    const user = await prisma.user.findUnique({
      where: {
        email: email
      }
    })

    if (!user) {
      throw new AppError('NOT_FOUND', 'ACCOUNT_NOT_FOUND')
    }
    const isPassword = await compare(password, user.password || '')

    if (!isPassword) {
      throw new AppError('BAD_REQUEST', 'INCORRECT_PASSWORD')
    }

    await prisma.user.update({
      where: {
        id: user.id
      },
      data: {
        last_login_at: now.toDate()
      }
    })

    const token = createJSONToken(user.id)
    return res.status(200).json(
      resultTemplate('SUCCESS', 'SUCCESS', {
        token: token,
        user: {
          id: user.id,
          name: user.name,
          sex: user.sex,
          date_of_birth: user.date_of_birth,
          email: user.email,
          phone_number: user.phone_number,
          image: user.image
        }
      })
    )
  } catch (error: any) {
    next(error)
  }
}

export const createRecipientInformation = async (req: Request, res: Response, next: NextFunction) => {
  const recipientInfo: ICreateRecipientInformation = req.body
  const user = req.user
  try {
    if (!user) throw new AppError('BAD_REQUEST', 'UNAUTHORIZED')

    const recipientExist = await prisma.recipient_information.findFirst({
      where: {
        user_id: user?.id
      }
    })
    const defaultRecipient: boolean = recipientExist ? false : true
    const result = await prisma.recipient_information.create({
      data: {
        name: recipientInfo.name,
        phone_number: recipientInfo.phone_number,
        address: recipientInfo.address,
        user_id: user?.id,
        default_recipient: defaultRecipient
      }
    })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', result))
  } catch (error: any) {
    next(error)
  }
}

export const getRecipientOfUser = async (req: Request, res: Response, next: NextFunction) => {
  const user = req.user
  try {
    const recipient = await prisma.recipient_information.findMany({
      where: {
        user_id: user?.id
      }
    })
    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', recipient))
  } catch (error: any) {
    next(error)
  }
}

export const updateRecipientInformation = async (req: Request, res: Response, next: NextFunction) => {
  const newInfomation: IUpdateRecipientInformation = req.body
  const user = req.user
  try {
    const recipientInfo = await prisma.recipient_information.findFirst({
      where: {
        user_id: user?.id,
        id: newInfomation.id_recipient
      }
    })

    if (!recipientInfo) {
      throw new AppError('BAD_REQUEST', 'THIS_IS_NOT_YOURS')
    }

    if (newInfomation.default_recipient) {
      await prisma.recipient_information.updateMany({
        where: {
          user_id: user?.id,
          default_recipient: true
        },
        data: {
          default_recipient: false
        }
      })
    }
    const result = await prisma.recipient_information.update({
      where: {
        id: newInfomation.id_recipient
      },
      data: {
        name: newInfomation.name,
        phone_number: newInfomation.phone_number,
        address: newInfomation.address,
        default_recipient: newInfomation.default_recipient
      }
    })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', result))
  } catch (error: any) {
    next(error)
  }
}

export const removeRecipientInformation = async (req: Request, res: Response, next: NextFunction) => {
  const { id_recipient } = req.query as IRemoveRecipientInformation
  const user = req.user
  try {
    const recipientInfo = await prisma.recipient_information.findFirst({
      where: {
        user_id: user?.id,
        id: parseInt(id_recipient)
      }
    })
    if (!recipientInfo) {
      throw new AppError('BAD_REQUEST', 'THIS_IS_NOT_YOURS')
    }
    await prisma.recipient_information.delete({
      where: {
        id: parseInt(id_recipient)
      }
    })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const getUserDetail = async (req: Request, res: Response, next: NextFunction) => {
  const { user_id } = req.query as IGetUserDetail
  try {
    const user = await prisma.user.findFirst({
      where: {
        id: parseInt(user_id)
      },
      include: {
        recipients: true
      }
    })
    if (!user) {
      throw new AppError('BAD_REQUEST', 'USER_NOT_FOUND')
    }
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', user))
  } catch (error: any) {
    next(error)
  }
}

export const getUserInformation = async (req: Request, res: Response, next: NextFunction) => {
  const user = req.user
  try {
    if (!user) {
      throw new AppError('NOT_FOUND', 'USER_NOT_FOUND')
    }
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', user))
  } catch (error: any) {
    next(error)
  }
}

export const updateUserInformation = async (req: Request, res: Response, next: NextFunction) => {
  const user = req.user
  const data = req.body as IUpdateUserInformation
  try {
    if (!user) {
      throw new AppError('NOT_FOUND', 'USER_NOT_FOUND')
    }
    const checkPhoneNumber = await prisma.user.findFirst({
      where: {
        phone_number: data.phone_number,
        NOT: {
          id: user.id
        }
      }
    })

    if (checkPhoneNumber && checkPhoneNumber.id != user.id) {
      throw new AppError('BAD_REQUEST', 'PHONE_NUMBER_EXISTED')
    }
    console.log(data.date_of_birth)
    const result = await prisma.user.update({
      select: {
        image: true,
        date_of_birth: true,
        email: true,
        id: true,
        name: true,
        last_login_at: true,
        phone_number: true,
        sex: true
      },
      where: {
        id: user.id
      },
      data: {
        ...data,
        date_of_birth: data.date_of_birth ? dayjs(data.date_of_birth).toDate() : null
      }
    })

    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', result))
  } catch (error: any) {
    next(error)
  }
}

export const updateUserImage = async (req: Request, res: Response, next: NextFunction) => {
  const user = req.user
  const file = req.files as Express.MulterS3.File[]
  try {
    if (!user) {
      throw new AppError('NOT_FOUND', 'USER_NOT_FOUND')
    }
    const checkImage = await prisma.user.findFirst({
      where: {
        id: user.id
      }
    })
    if (checkImage && checkImage.image !== null) {
      deleteImage(checkImage.image)
    }
    const result = await prisma.user.update({
      select: {
        image: true,
        date_of_birth: true,
        email: true,
        id: true,
        name: true,
        last_login_at: true,
        phone_number: true,
        sex: true
      },
      where: {
        id: user.id
      },
      data: {
        image: file[0].location
      }
    })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', result))
  } catch (error: any) {
    next(error)
  }
}

export const getOtpForgotPassword = async (req: Request, res: Response, next: NextFunction) => {
  const { email } = req.body as IGetOtpForgotPassword
  try {
    const user = await prisma.user.findFirst({
      where: {
        email
      }
    })
    if (!user) {
      throw new AppError('NOT_FOUND', 'ACCOUNT_NOT_FOUND')
    }
    const otpCode = Math.floor(1000 + Math.random() * 9000).toString()
    const otpExpireAt = dayjs().add(20, 'minute').toDate()
    const userInfor = await prisma.user.update({
      where: {
        id: user.id
      },
      data: {
        otp_code: otpCode,
        otp_expired_at: otpExpireAt
      }
    })
    resetPwdUserOtpService(userInfor.email, otpCode, userInfor.name)
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const changePassword = async (req: Request, res: Response, next: NextFunction) => {
  const user = req.user
  const { newPassword, oldPassword } = req.body as IChangePassword
  try {
    const userPassword = await prisma.user.findFirst({
      where: {
        id: user?.id
      }
    })
    if (!userPassword) {
      throw new AppError('NOT_FOUND', 'USER_NOT_FOUND')
    }
    const isPassword = await compare(oldPassword, userPassword.password || '')
    if (!isPassword) {
      throw new AppError('BAD_REQUEST', 'INCORRECT_PASSWORD')
    }
    const hashedPw = await hash(newPassword, 12)
    const newUserInfor = await prisma.user.update({
      where: {
        id: userPassword.id
      },
      data: {
        password: hashedPw,
        otp_code: null,
        otp_expired_at: null,
        password_change_at: new Date()
      }
    })
    resetPwdUserSuccessService(newUserInfor.email, newUserInfor.name, newUserInfor.password_change_at)
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', newUserInfor))
  } catch (error: any) {
    next(error)
  }
}

export const verifyOtpCode = async (req: Request, res: Response, next: NextFunction) => {
  const { email, otp_code } = req.body as IVerifyOtpCode
  try {
    const userInfor = await prisma.user.findFirst({
      where: {
        email
      }
    })
    if (!userInfor) {
      throw new AppError('NOT_FOUND', 'ACCOUNT_NOT_FOUND')
    }
    if (otp_code != userInfor?.otp_code) {
      throw new AppError('BAD_REQUEST', 'INVALID_OTP_CODE')
    }
    if (dayjs().isAfter(dayjs(userInfor.otp_expired_at))) {
      throw new AppError('BAD_REQUEST', 'OTP_CODE_EXPIRED')
    }
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const resetPasswordForgot = async (req: Request, res: Response, next: NextFunction) => {
  const { email, otp_code, newPassword } = req.body as IResetPasswordForgot
  try {
    const userInfor = await prisma.user.findFirst({
      where: {
        email
      }
    })
    if (!userInfor) {
      throw new AppError('NOT_FOUND', 'ACCOUNT_NOT_FOUND')
    }
    if (otp_code != userInfor?.otp_code) {
      throw new AppError('BAD_REQUEST', 'INVALID_OTP_CODE')
    }
    if (dayjs().isAfter(dayjs(userInfor.otp_expired_at))) {
      throw new AppError('BAD_REQUEST', 'OTP_CODE_EXPIRED')
    }
    const hashedPw = await hash(newPassword, 12)
    const newUserInfor = await prisma.user.update({
      where: {
        id: userInfor.id
      },
      data: {
        password: hashedPw,
        otp_code: null,
        otp_expired_at: null,
        password_change_at: new Date()
      }
    })
    resetPwdUserSuccessService(newUserInfor.email, newUserInfor.name, newUserInfor.password_change_at)
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const signInWithGoogle = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { googleToken } = req.body
    if (!googleToken || !googleToken.length) throw new AppError('BAD_REQUEST', 'GOOGLE_TOKEN_REQUIRED')
    let userInfo = await getUserInfoGoogle(googleToken)
    if ('error' in userInfo) {
      throw new AppError('BAD_REQUEST', 'GOOGLE_TOKEN_INVALID')
    }
    let user = await prisma.user.findFirst({
      where: {
        email: userInfo.email
      }
    })
    if (user) {
      await prisma.user.update({
        data: {
          image: userInfo.picture,
          name: userInfo.name
        },
        where: {
          email: userInfo.email
        }
      })
    } else {
      ;(user = await prisma.user.create({
        data: {
          image: userInfo.picture,
          name: userInfo.name,
          email: userInfo.email
        }
      })),
        await session.run('MERGE (u:User {id: $id, email: $email})', { id: user.id, email: user.email })
    }
    //Trả về token để đăng nhập nhanh
    const token = createJSONToken(user.id)
    return res.json(
      resultTemplate('SUCCESS', 'SUCCESS', {
        token,
        user: { id: user.id, name: user.name, email: user.email, phone_number: user.phone_number }
      })
    )
  } catch (error) {
    next(error)
  }
}
