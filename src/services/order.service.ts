import prisma from '@/libs/prisma'
import dayjs from 'dayjs'

interface IGetOrderWithStateServiceForUser {
  state?: string
  user_id?: number
  page?: number
  limit?: number
}

export const getOrderWithStateServiceForUser = async ({
  user_id,
  limit,
  page,
  state
}: IGetOrderWithStateServiceForUser) => {
  let result = await prisma.order.findMany({
    where: {
      recipient_information: {
        user_id: user_id
      },
      state: state
    },
    skip: page ? (page - 1) * (limit ? limit : 10) : undefined,
    take: limit || undefined,
    orderBy: {
      created_at: 'desc'
    },
    select: {
      id: true,
      amount: true,
      created_at: true,
      payment_method: true,
      payment_link: true,
      transaction_at: true,
      order_detail: {
        select: {
          quantity: true,
          price: true,
          variant: {
            select: {
              color: true,
              size: true,
              price_sale: true,
              price_export: true,
              product: {
                select: {
                  id: true,
                  name: true,
                  product_image: true
                }
              }
            }
          }
        },
        take: 1
      }
    }
  })
  result.map((order, i) => {
    const orderDetail = order.order_detail
    orderDetail.map((detail, j) => {
      result[i].order_detail[j].variant.product.product_image = [
        detail.variant.product.product_image.filter((image) => image.color_id === detail.variant.color.id)[0]
      ]
    })
  })
  return result
}

interface IGetOrderWithStateServiceForAdmin {
  state?: string
  page?: number
  limit?: number
  user_id?: number
}

export const getOrderWithStateServiceForAdmin = async ({
  limit,
  page,
  state,
  user_id
}: IGetOrderWithStateServiceForAdmin) => {
  let result = await prisma.order.findMany({
    where: {
      state: state,
      recipient_information: {
        user_id: user_id
      }
    },
    skip: page ? (page - 1) * (limit ? limit : 10) : undefined,
    take: limit || undefined,
    orderBy: {
      created_at: 'desc'
    }
  })
  return result
}

interface OrderItem {
  variant_id: number
  quantity: number
  price: number
}

export const createOrderService = async (variant_order: OrderItem[]) => {
  const currentYear = dayjs().year()
  const currentMonth = dayjs().month() + 1
  await Promise.all(
    variant_order.map(async (variant) => {
      const checkProductAnalysis = await prisma.analyze_product_monthly.findFirst({
        where: {
          product: {
            variants: {
              some: {
                id: variant.variant_id
              }
            }
          },
          month: currentMonth,
          year: currentYear
        }
      })
      // records have not been created for this month
      if (!checkProductAnalysis) {
        const product = await prisma.product.findFirst({
          where: {
            variants: {
              some: {
                id: variant.variant_id
              }
            }
          },
          select: {
            id: true
          }
        })
        if (!product) {
          return
        }
        await prisma.analyze_product_monthly.create({
          data: {
            year: currentYear,
            month: currentMonth,
            product_id: product.id,
            quantity: variant.quantity
          }
        })
      } else {
        await prisma.analyze_product_monthly.update({
          where: {
            id: checkProductAnalysis.id
          },
          data: {
            quantity: {
              increment: variant.quantity
            }
          }
        })
      }
    })
  )
}

export const cancelOrderService = async (order_id: number, month: number, year: number) => {
  const variantArray = await prisma.order_detail.findMany({
    where: {
      order_id: order_id
    }
  })
  await Promise.all(
    variantArray.map(async (variant) => {
      const checkProductAnalysis = await prisma.analyze_product_monthly.findFirst({
        where: {
          product: {
            variants: {
              some: {
                id: variant.variant_id
              }
            }
          },
          month: month,
          year: year
        }
      })
      await prisma.analyze_product_monthly.update({
        where: {
          id: checkProductAnalysis?.id
        },
        data: {
          quantity: {
            decrement: variant.quantity
          }
        }
      })
    })
  )
}
