import { Static, Type } from '@sinclair/typebox'
import {
  addressSchema,
  emailSchema,
  nameSchema,
  numberSchema,
  passwordSchema,
  phoneNumberSchema
} from './common.schema'

export const createOrderSchema = Type.Object({
  description: Type.Optional(Type.String()),
  webhook_data: Type.Optional(Type.String()),
  amount: Type.Number(),
  recipient_information_id: Type.Number(),
  shipping_method_id: Type.Number(),
  payment_method_id: Type.Number(),
  voucher_id: Type.Optional(Type.Number()),
  variant_order: Type.Array(
    Type.Object({
      variant_id: Type.Number(),
      quantity: Type.Number(),
      price: Type.Number()
    })
  )
})
export type ICreateOrder = Static<typeof createOrderSchema>

export const getOrderDetailSchema = Type.Object({
  order_id: numberSchema
})
export type IGetOrderDetail = Static<typeof getOrderDetailSchema>

export const updateOrderStateSchema = Type.Object({
  order_id: numberSchema,
  state: Type.String()
})
export type IUpdateOrderState = Static<typeof updateOrderStateSchema>

export const userCancelOrderSchema = Type.Object({
  order_id: numberSchema
})
export type IUserCancelOrder = Static<typeof userCancelOrderSchema>

export const payosHandlePaymentSchema = Type.Object({
  code: Type.String(),
  desc: Type.String(),
  success: Type.Boolean(),
  data: Type.Object({
    orderCode: Type.Number(),
    amount: Type.Number(),
    description: Type.String(),
    accountNumber: Type.String(),
    reference: Type.String(),
    transactionDateTime: Type.String(),
    currency: Type.String(),
    paymentLinkId: Type.String(),
    code: Type.String(),
    desc: Type.String(),
    counterAccountBankId: Type.Optional(Type.String()),
    counterAccountBankName: Type.Optional(Type.String()),
    counterAccountName: Type.Optional(Type.String()),
    counterAccountNumber: Type.Optional(Type.String()),
    virtualAccountName: Type.Optional(Type.String()),
    virtualAccountNumber: Type.Optional(Type.String())
  }),
  signature: Type.String()
})
export type IPayosHandlePayment = Static<typeof payosHandlePaymentSchema>

export const getOrderOfUserSchema = Type.Object({
  state: Type.Optional(Type.String()),
  page: Type.Optional(numberSchema),
  limit: Type.Optional(numberSchema)
})
export type IGetOrderOfUser = Static<typeof getOrderOfUserSchema>

export const getOrderAdminSchema = Type.Object({
  state: Type.Optional(Type.String()),
  page: Type.Optional(numberSchema),
  limit: Type.Optional(numberSchema),
  user_id: Type.Optional(Type.String())
})
export type IGetOrderAdmin = Static<typeof getOrderAdminSchema>

export const orderDetailOfUserNotYetReviewSchema = Type.Object({
  limit: Type.Optional(numberSchema),
  page: Type.Optional(numberSchema)
})
export type IOrderDetailOfUserNotYetReview = Static<typeof orderDetailOfUserNotYetReviewSchema>

export const updatePaymentMethodSchema = Type.Object({
  order_id: Type.Number(),
  payment_method_id: Type.Number()
})

export type IUpdatePaymentMethod = Static<typeof updatePaymentMethodSchema>
