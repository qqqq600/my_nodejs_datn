import FakeReviewController from '@/controllers/fake-review.controller'
import express, { Express } from 'express'
const router = express.Router()

const FakeReviewRoute = (app: Express) => {
  router.post('/import', FakeReviewController.importReviews)
  router.get('/products', FakeReviewController.getAllProducts)
  router.get('/products/:id', FakeReviewController.getReviewsByProductId)
  return app.use('/fake-review', router)
}

export default FakeReviewRoute
