import { getRecommendProductFromOtherUser } from './../controllers/neo4j.controller'
import express, { Express } from 'express'
import validate from '@/middlewares/validate.middleware'
import { validateUser } from '@/middlewares/user.middleware'
import { uploadImage } from './../services/s3.service'
import {
  changePasswordSchema,
  createRecipientInformationSchema,
  createUserSchema,
  getOtpForgotPasswordSchema,
  getUserDetailSchema,
  removeRecipientInformationSchema,
  resetPasswordForgotSchema,
  signInWithGoogleSchema,
  updateRecipientInformationSchema,
  updateUserInformationSchema,
  userLoginSchema,
  verifyOtpCodeSchema
} from '@/schema/user.schema'
import {
  changePassword,
  createRecipientInformation,
  createUser,
  getAllUser,
  getOtpForgotPassword,
  getRecipientOfUser,
  getUserDetail,
  getUserInformation,
  removeRecipientInformation,
  resetPasswordForgot,
  signInWithGoogle,
  updateRecipientInformation,
  updateUserImage,
  updateUserInformation,
  userLogin,
  verifyOtpCode
} from '@/controllers/user.controller'
import {
  addToCart,
  addToWish,
  addViewedProduct,
  deleteFromCart,
  deleteFromWish,
  getFromCart,
  getFromWish,
  getRecommendProductFromUser,
  getViewedProduct,
  updateToCart
} from '@/controllers/neo4j.controller'
import {
  addToCartSchema,
  addToWishSchema,
  addViewedProductSchema,
  deleteFromCartSchema,
  deleteFromWishSchema,
  getRecommendProductFromUserSchema,
  getViewedProductSchema,
  updateToCartSchema
} from '@/schema/neo4j.schema'

const router = express.Router()

const UserRoute = (app: Express) => {
  router.get('/', getAllUser)
  router.post('/create', validate({ schema: createUserSchema, part: 'body' }), createUser)
  router.post('/login', validate({ schema: userLoginSchema, part: 'body' }), userLogin)
  router.get('/detail', validate({ schema: getUserDetailSchema, part: 'query' }), getUserDetail)
  router.post('/forgot-pwd', validate({ schema: getOtpForgotPasswordSchema, part: 'body' }), getOtpForgotPassword)
  router.post('/verify-otp', validate({ schema: verifyOtpCodeSchema, part: 'body' }), verifyOtpCode)
  router.patch('/reset-pwd-forgot', validate({ schema: resetPasswordForgotSchema, part: 'body' }), resetPasswordForgot)
  router.post('/login/google', validate({ part: 'body', schema: signInWithGoogleSchema }), signInWithGoogle)

  router.use(validateUser)
  router.get('/infor', getUserInformation)
  router.patch('/infor', validate({ schema: updateUserInformationSchema, part: 'body' }), updateUserInformation)
  router.patch('/image', uploadImage('user', 1).array('image'), updateUserImage)
  router.patch('/change-password', validate({ schema: changePasswordSchema, part: 'body' }), changePassword)
  router.get('/recipient', getRecipientOfUser)
  router.post(
    '/recipient',
    validate({ schema: createRecipientInformationSchema, part: 'body' }),
    createRecipientInformation
  )
  router.patch(
    '/recipient',
    validate({ schema: updateRecipientInformationSchema, part: 'body' }),
    updateRecipientInformation
  )
  router.delete(
    '/recipient',
    validate({ schema: removeRecipientInformationSchema, part: 'query' }),
    removeRecipientInformation
  )
  // Cart
  router.get('/cart', getFromCart)
  router.post('/cart', validate({ schema: addToCartSchema, part: 'body' }), addToCart)
  router.patch('/cart', validate({ schema: updateToCartSchema, part: 'body' }), updateToCart)
  router.delete('/cart', validate({ schema: deleteFromCartSchema, part: 'query' }), deleteFromCart)

  // Wish
  router.get('/wish', getFromWish)
  router.post('/wish', validate({ schema: addToWishSchema, part: 'body' }), addToWish)
  router.delete('/wish', validate({ schema: deleteFromWishSchema, part: 'query' }), deleteFromWish)

  // History View product
  router.get('/viewed', validate({ schema: getViewedProductSchema, part: 'query' }), getViewedProduct)
  router.post('/viewed', validate({ schema: addViewedProductSchema, part: 'body' }), addViewedProduct)

  // Recommendation by user
  router.get(
    '/recommendbyuser',
    validate({ part: 'query', schema: getRecommendProductFromUserSchema }),
    getRecommendProductFromUser
  )
  router.get(
    '/recommendbyotheruser',
    validate({ part: 'query', schema: getRecommendProductFromUserSchema }),
    getRecommendProductFromOtherUser
  )
  return app.use('/user', router)
}

export default UserRoute
