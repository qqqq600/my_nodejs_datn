import { Static, Type } from '@sinclair/typebox'
import {
  addressSchema,
  emailSchema,
  nameSchema,
  numberSchema,
  passwordSchema,
  phoneNumberSchema
} from './common.schema'

export const createReviewSchema = Type.Object({
  rating: numberSchema,
  content: Type.String(),
  order_detail_id: numberSchema
})
export type ICreateReview = Static<typeof createReviewSchema>

export const deleteReviewSchema = Type.Object({
  review_id: numberSchema
})
export type IDeleteReview = Static<typeof deleteReviewSchema>

export const getReviewSchema = Type.Object({
  product_id: Type.Optional(numberSchema),
  user_id: Type.Optional(numberSchema),
  page: Type.Optional(numberSchema),
  limit: Type.Optional(numberSchema),
  is_disable: Type.Optional(Type.Union([Type.Literal('true'), Type.Literal('false')]))
})
export type IGetReview = Static<typeof getReviewSchema>

export const getReviewOfUserSchema = Type.Object({
  page: Type.Optional(numberSchema),
  is_disable: Type.Optional(Type.Union([Type.Literal('true'), Type.Literal('false')])),
  limit: Type.Optional(numberSchema)
})
export type IGetReviewOfUser = Static<typeof getReviewOfUserSchema>


export const adminUpdateReviewSchema = Type.Object({
  review_id: Type.Number(),
  is_disable: Type.Boolean()
})
export type IAdminUpdateReview = Static<typeof adminUpdateReviewSchema>
