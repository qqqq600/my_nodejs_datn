import { Static, Type } from '@sinclair/typebox'
import {
  addressSchema,
  emailSchema,
  nameSchema,
  numberSchema,
  passwordSchema,
  phoneNumberSchema
} from './common.schema'

export const createUserSchema = Type.Object({
  name: Type.Optional(nameSchema),
  email: nameSchema,
  password: passwordSchema,
  phone_number: phoneNumberSchema
})

export type ICreateUser = Static<typeof createUserSchema>

export const userLoginSchema = Type.Object({
  email: emailSchema,
  password: passwordSchema
})

export type IUserLogin = Static<typeof userLoginSchema>

export const createRecipientInformationSchema = Type.Object({
  name: nameSchema,
  address: addressSchema,
  phone_number: phoneNumberSchema
})

export type ICreateRecipientInformation = Static<typeof createRecipientInformationSchema>

export const updateRecipientInformationSchema = Type.Object({
  id_recipient: Type.Integer(),
  name: nameSchema,
  address: addressSchema,
  phone_number: phoneNumberSchema,
  default_recipient: Type.Boolean()
})

export type IUpdateRecipientInformation = Static<typeof updateRecipientInformationSchema>

export const removeRecipientInformationSchema = Type.Object({
  id_recipient: Type.String()
})

export type IRemoveRecipientInformation = Static<typeof removeRecipientInformationSchema>

export const getUserDetailSchema = Type.Object({
  user_id: numberSchema
})
export type IGetUserDetail = Static<typeof getUserDetailSchema>

export const updateUserInformationSchema = Type.Object({
  name: nameSchema,
  phone_number: phoneNumberSchema,
  sex: Type.String(),
  date_of_birth: Type.String()
})
export type IUpdateUserInformation = Static<typeof updateUserInformationSchema>

export const getOtpForgotPasswordSchema = Type.Object({
  email: emailSchema
})
export type IGetOtpForgotPassword = Static<typeof getOtpForgotPasswordSchema>

export const changePasswordSchema = Type.Object({
  oldPassword: passwordSchema,
  newPassword: passwordSchema
})
export type IChangePassword = Static<typeof changePasswordSchema>

export const resetPasswordForgotSchema = Type.Object({
  email: emailSchema,
  newPassword: passwordSchema,
  otp_code: Type.String()
})
export type IResetPasswordForgot = Static<typeof resetPasswordForgotSchema>

export const verifyOtpCodeSchema = Type.Object({
  email: emailSchema,
  otp_code: Type.String()
})
export type IVerifyOtpCode = Static<typeof verifyOtpCodeSchema>

export const signInWithGoogleSchema = Type.Object({
  googleToken: Type.String()
})
export type ISignInWithGoogle = Static<typeof signInWithGoogleSchema>
