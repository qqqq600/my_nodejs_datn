import { NextFunction, Request, Response } from 'express'
import {
  ICreateProduct,
  ICreateVariant,
  IGetProduct,
  IGetProductDetail,
  IUpdateProduct,
  IGetVariant,
  IUpdateVariant,
  ICreateVariantImage
} from '@/schema/product.schema'
import prisma from '@/libs/prisma'
import { resultTemplate } from '@/utils/result-template'
import { VARIANT_SIZE } from '@/config/constants'
import { AppError } from '@/config/appError'
import { session } from '@/neo4j/configDbNeo4j'
import { deleteImage } from '@/services/s3.service'
import { deleteFilesImageService, getProductService, uploadFilesImageService } from '@/services/product.service'

export const createProduct = async (req: Request, res: Response, next: NextFunction) => {
  const data: ICreateProduct = req.body
  try {
    const product = await prisma.product.create({
      data: {
        ...data
      }
    })

    await session.run(
      'MERGE (p:Product {id: $product_id, name: $name}) WITH p MATCH (c:Category {id: $category_id}) MERGE (p)-[:IS_IN]->(c) ',
      { product_id: product.id, name: product.name, category_id: product.category_id }
    )

    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', { product_id: product.id }))
  } catch (error: any) {
    next(error)
  }
}

export const updateProduct = async (req: Request, res: Response, next: NextFunction) => {
  const data: IUpdateProduct = req.body

  try {
    const existingProduct = await prisma.product.findFirst({
      where: {
        id: data.id
      }
    })

    if (!existingProduct) {
      throw new AppError('NOT_FOUND', 'NOT_FOUND')
    }

    const updatedProduct = await prisma.product.update({
      where: {
        id: data.id
      },
      data
    })

    await session.run(
      'MATCH (p:Product {id: $id})-[r:IS_IN]->() DELETE r WITH p MATCH (c:Category {id: $category_id}) CREATE (p)-[:IS_IN]->(c)',
      { id: updatedProduct.id, category_id: updatedProduct.category_id }
    )

    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const deleteProduct = async (req: Request, res: Response, next: NextFunction) => {
  const id = parseInt(req.query.id as string)
  try {
    const product = await prisma.product.findFirst({
      include: {
        product_image: true
      },
      where: {
        id
      }
    })
    if (!product) {
      throw new AppError('NOT_FOUND', 'PRODUCT_NOT_EXISTED')
    }
    const productHasOrder = await prisma.order_detail.findFirst({
      where: {
        variant: {
          product_id: id
        }
      }
    })
    if (productHasOrder) {
      throw new AppError('BAD_REQUEST', 'PRODUCT_HAS_ORDER')
    }
    console.log(productHasOrder)
    await prisma.variant.deleteMany({
      where: {
        product_id: id
      }
    })
    await prisma.product_image.deleteMany({
      where: {
        product_id: id
      }
    })
    product.product_image.map(async (item) => {
      await deleteImage(item.image)
    })

    await prisma.product.delete({
      where: {
        id
      }
    })

    await session.run('MATCH (p:Product {id: $id})-[r:HAS]->(v:Variant) DETACH DELETE p, v, r', { id })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const getVariant = async (req: Request, res: Response, next: NextFunction) => {
  const data = req.query as IGetVariant
  const product_id = data.product_id ? parseInt(data.product_id) : undefined
  const color_id = data.color_id ? parseInt(data.color_id) : undefined
  const size = data.size
  try {
    const variants = await prisma.variant.findMany({
      where: {
        product_id,
        color_id,
        size
      },
      include: {
        color: true,
        product: {
          include: {
            product_image: {
              select: {
                image: true,
                color_id: true,
                id: true
              },
              where: {
                color_id
              }
            }
          }
        }
      }
    })
    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', variants))
  } catch (error: any) {
    next(error)
  }
}

export const createVariant = async (req: Request, res: Response, next: NextFunction) => {
  const body: ICreateVariant = req.body
  const { size } = body
  const color_id = parseInt(body.color_id)
  const product_id = parseInt(body.product_id)
  const price_export = body.price_export ? parseInt(body.price_export) : null
  const price_import = body.price_import ? parseInt(body.price_import) : null
  const price_sale = body.price_sale ? parseInt(body.price_sale) : null
  const remain_quantity = body.remain_quantity ? parseInt(body.remain_quantity) : null
  const file = req.files as Express.MulterS3.File[]
  try {
    if (size && VARIANT_SIZE.includes(size) === false) {
      throw new AppError('BAD_REQUEST', 'INVALID_VARIANT_SIZE')
    }
    if (price_export && price_sale && price_export < price_sale) {
      throw new AppError('BAD_REQUEST', 'INVALID_PRICE')
    }
    const existedProduct = await prisma.product.findFirst({ where: { id: product_id } })
    if (!existedProduct) {
      throw new AppError('BAD_REQUEST', 'PRODUCT_NOT_EXISTED')
    }

    const exitstedVariant = await prisma.variant.findFirst({
      where: {
        color_id: color_id,
        product_id: product_id,
        size: size
      }
    })

    if (exitstedVariant) {
      throw new AppError('BAD_REQUEST', 'VARIANT_EXISTED')
    }
    const percent = price_export && price_sale ? ((price_export - price_sale) / price_export) * 100 : 0
    const roundedPercent = parseFloat(percent.toFixed(2))

    const variant = await prisma.variant.create({
      data: {
        price_export: price_export,
        price_import: price_import,
        price_sale: price_sale,
        percent_sale: roundedPercent,
        remain_quantity: remain_quantity,
        size: size,
        updated_at: new Date(),
        color_id: color_id,
        product_id: product_id
      }
    })

    let locations: string[] = []
    let images_id: string[] = []
    await session.run(
      'MERGE (v:Variant {id: $variant_id, quantity: $quantity}) WITH v MATCH (p:Product {id: $product_id}) MERGE (p)-[:HAS]->(v) ',
      { variant_id: variant.id, quantity: variant.remain_quantity, product_id: variant.product_id }
    )
    
    if (file.length > 0) {
      await Promise.all(
        file.map(async (item) => {
          const image = await prisma.product_image.create({
            data: {
              image: item.location,
              color_id: variant.color_id,
              product_id: variant.product_id
            }
          })
          images_id.push(image.id.toString())
          locations.push(item.location)
        })
      )

      await uploadFilesImageService(images_id, locations)
    }

    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    file.map((item) => {
      deleteImage(item.location)
    })
    next(error)
  }
}

export const updateVariant = async (req: Request, res: Response, next: NextFunction) => {
  const data: IUpdateVariant = req.body
  const { price_export, price_sale } = data

  try {
    if (data.size && VARIANT_SIZE.includes(data.size) === false) {
      throw new AppError('BAD_REQUEST', 'INVALID_VARIANT_SIZE')
    }
    if (price_export && price_sale && price_export < price_sale) {
      throw new AppError('BAD_REQUEST', 'INVALID_PRICE')
    }
    const existedVariant = await prisma.variant.findFirst({
      where: { id: data.id }
    })

    if (!existedVariant) {
      throw new AppError('BAD_REQUEST', 'VARIANT_NOT_EXISTED')
    }
    const percent = price_export && price_sale ? ((price_export - price_sale) / price_export) * 100 : 0
    const roundedPercent = parseFloat(percent.toFixed(2))

    const variant = await prisma.variant.update({
      where: {
        id: data.id
      },
      data: {
        ...data,
        percent_sale: roundedPercent,
        updated_at: new Date()
      }
    })

    await session.run(
      'MATCH (v:Variant {id: $variant_id}) <-[r:HAS]-() MATCH (p: Product {id: $product_id}) DELETE r SET v.quantity = $quantity MERGE (p) -[:HAS]-> (v)',
      {
        variant_id: variant.id,
        product_id: variant.product_id,
        quantity: variant.remain_quantity
      }
    )

    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const deleteVariant = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = parseInt(req.query.id as string)
    const variant = await prisma.variant.findFirst({ where: { id } })
    if (!variant) {
      throw new AppError('NOT_FOUND', 'VARIANT_NOT_EXISTED')
    }

    //check order detail exist ??
    const checkOrder = await prisma.order_detail.findFirst({
      where: {
        variant_id: id
      }
    })

    if (checkOrder) {
      throw new AppError('BAD_REQUEST', 'VARIANT_HAS_ORDER')
    }

    await prisma.variant.delete({ where: { id } })
    await session.run('MATCH (v:Variant {id: $id}) DETACH DELETE v', { id })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const createVariantImage = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.body as ICreateVariantImage
  const files = req.files as Express.MulterS3.File[]
  try {
    if (!files || files.length === 0) {
      throw new AppError('BAD_REQUEST', 'INVALID_PARAMS')
    }
    const variant = await prisma.variant.findFirst({ where: { id: parseInt(id) } })
    if (!variant) {
      files.map(async (item) => {
        await deleteImage(item.location)
      })
      throw new AppError('BAD_REQUEST', 'VARIANT_NOT_EXISTED')
    }
    let locations: string[] = []
    let images_id: string[] = []
    await Promise.all(
      files.map(async (item) => {
        const image = await prisma.product_image.create({
          data: {
            image: item.location,
            color_id: variant.color_id,
            product_id: variant.product_id
          }
        })
        images_id.push(image.id.toString())
        locations.push(item.location)
      })
    )
    await uploadFilesImageService(images_id, locations)
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const deleteVariantImage = async (req: Request, res: Response, next: NextFunction) => {
  const id = parseInt(req.query.id as string)
  try {
    const variantImage = await prisma.product_image.findFirst({ where: { id } })
    if (!variantImage) {
      throw new AppError('NOT_FOUND', 'NOT_FOUND')
    }
    await deleteImage(variantImage.image)
    await prisma.product_image.delete({ where: { id } })
    await deleteFilesImageService(id.toString())
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const getProducts = async (req: Request, res: Response, next: NextFunction) => {
  const { page, limit, search, category_id, rating, sort_by, max_price, min_price, product_id, voucher_id, image } =
    req.body as IGetProduct
  try {
    const products = await getProductService({
      category_id: category_id,
      limit: limit ? limit : 10,
      page: page ? page : 1,
      rating: rating,
      search: search,
      image: image,
      voucher_id: voucher_id,
      sort_by: sort_by,
      product_id: product_id,
      min_price: min_price,
      max_price: max_price
    })

    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', products))
  } catch (error: any) {
    next(error)
  }
}

export const getProductDetailData = async (req: Request, res: Response, next: NextFunction) => {
  const { product_id } = req.query as IGetProductDetail
  try {
    const count_bought = await prisma.order_detail.count({ where: { variant: { product_id: parseInt(product_id) } } })
    const count_review = await prisma.review.count({
      where: { is_disable: false, order_detail: { variant: { product_id: parseInt(product_id) } } }
    })
    const productData = await prisma.product.findFirst({
      where: {
        id: parseInt(product_id)
      },
      include: {
        variants: {
          include: {
            color: true
          }
        },
        product_image: {
          select: {
            id: true,
            color_id: true,
            image: true
          }
        }
      }
    })
    if (!productData) {
      throw new AppError('NOT_FOUND', 'PRODUCT_NOT_EXISTED')
    }

    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', { ...productData, count_bought, count_review }))
  } catch (error: any) {
    next(error)
  }
}

export const getColor = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const colors = await prisma.color.findMany()
    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', colors))
  } catch (error: any) {
    next(error)
  }
}
