import { Static, Type } from '@sinclair/typebox'
import {
  addressSchema,
  emailSchema,
  nameSchema,
  numberSchema,
  passwordSchema,
  phoneNumberSchema
} from './common.schema'

export const deleteUserSchema = Type.Object({
  user_id: numberSchema
})
export type IDeleteUser = Static<typeof deleteUserSchema>

export const staffLoginSchema = Type.Object({
  email: emailSchema,
  password: passwordSchema
})

export type IStaffLogin = Static<typeof staffLoginSchema>

export const createStaffSchema = Type.Object({
  name: nameSchema,
  email: emailSchema,
  password: passwordSchema,
  phone_number: phoneNumberSchema,
  role: nameSchema
})

export type ICreateStaff = Static<typeof createStaffSchema>

export const getStaffDetailSchema = Type.Object({
  staff_id: numberSchema
})
export type IGetStaffDetail = Static<typeof getStaffDetailSchema>

export const getRevenueSchema = Type.Object({
  mode: nameSchema,
  startDateInput: Type.Optional(Type.String()),
  endDateInput: Type.Optional(Type.String()),
})
export type IGetRevenue = Static<typeof getRevenueSchema>

export const getBestSellingSchema = Type.Object({
  mode: nameSchema,
  startDateInput: Type.Optional(Type.String()),
  endDateInput: Type.Optional(Type.String()),
})
export type IGetBestSelling = Static<typeof getBestSellingSchema>
