FROM node:16

# Thiết lập thư mục làm việc trong container
WORKDIR /app

# Sao chép package.json và package-lock.json vào thư mục /app

# Cài đặt các dependencies
COPY . ./app

RUN npm install

# Sao chép mã nguồn ứng dụng vào thư mục /app

# Mở cổng 3000 để ứng dụng Node.js lắng nghe
EXPOSE 3000

# Khởi chạy ứng dụng
CMD [ "node", "/src/server.ts" ]