import express, { Express } from 'express'
import { adminUpdateReview, createReview, deleteReview, getReview, getReviewOfUser } from '../controllers/review.controller'
import validate from '@/middlewares/validate.middleware'
import { validateUser } from '@/middlewares/user.middleware'
import { adminUpdateReviewSchema, createReviewSchema, deleteReviewSchema, getReviewOfUserSchema, getReviewSchema } from '@/schema/review.schema'
import { validateAdmin } from '@/middlewares/admin.middlewate'
import { uploadImage } from '@/services/s3.service'
const router = express.Router()

const ReviewRoute = (app: Express) => {
  router.post(
    '/',
    validateUser,
    uploadImage('review', 1).array('image'),
    validate({ schema: createReviewSchema, part: 'body' }),
    createReview
  )
  router.delete('/', validate({ schema: deleteReviewSchema, part: 'query' }), deleteReview)
  router.get('/', validate({ schema: getReviewSchema, part: 'query' }), getReview)
  router.get('/user', validateUser, validate({ schema: getReviewOfUserSchema, part: 'query' }), getReviewOfUser)
  router.patch('/is-disable', validateAdmin, validate({schema: adminUpdateReviewSchema, part: 'body'}), adminUpdateReview)

  return app.use('/review', router)
}

export default ReviewRoute
