import { AppError } from '@/config/appError'
import prisma from '@/libs/prisma'
import { session } from '@/neo4j/configDbNeo4j'
import {
  IAddToCart,
  IAddToWish,
  IAddViewedProduct,
  IDeleteFromCart,
  IDeleteFromWish,
  IGetRecommendProductFromUser,
  IGetViewedProduct,
  IUpdateToCart
} from '@/schema/neo4j.schema'
import { resultTemplate } from '@/utils/result-template'
import { Product } from '@prisma/client'
import dayjs from 'dayjs'
import { NextFunction, Request, Response } from 'express'

export const getFromCart = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = req.user
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    const data = await session.run(
      'MATCH (u:User {id: $id})-[r:IN_CART]->(v:Variant) RETURN v, r ORDER BY r.created_at DESC',
      { id: user.id }
    )
    const variants_id = data.records.map((record) => record.get('v').properties.id)
    const quantities = data.records.map((record) => record.get('r').properties.quantity)
    const result: any[] = []
    for (let i = 0; i < variants_id.length; i++) {
      const variant = await prisma.variant.findUnique({
        select: {
          id: true,
          color_id: true,
          price_export: true,
          price_sale: true,
          remain_quantity: true,
          color: {
            select: {
              id: true,
              color_code: true,
              name: true
            }
          },
          size: true,
          product_id: true,
          product: {
            select: {
              name: true,
              product_image: {
                select: {
                  image: true,
                  color_id: true
                }
              }
            }
          }
        },
        where: {
          id: variants_id[i]
        }
      })
      if (variant) {
        variant.product.product_image = variant.product.product_image.filter(
          (image) => image.color_id === variant.color_id
        )
      }
      result.push({ ...variant, quantity: quantities[i] })
    }
    return res.json(resultTemplate('SUCCESS', 'SUCCESS', result))
  } catch (error: any) {
    next(error)
  }
}

export const updateToCart = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = req.user
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    const { quantity, variant_id } = req.body as IUpdateToCart
    await session.run(
      'MATCH (u:User {id: $user_id})-[r:IN_CART]->(v:Variant {id: $variant_id}) SET r.quantity = $quantity RETURN r',
      { user_id: user.id, variant_id: variant_id, quantity: quantity }
    )
    return res.json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const addToCart = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = req.user
    const { quantity, variant_id } = req.body as IAddToCart
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    const existed = await session.run(
      'MATCH (u:User {id: $user_id})-[r:IN_CART]->(v:Variant {id: $variant_id}) RETURN r',
      { user_id: user.id, variant_id }
    )
    if (existed.records.length > 0) {
      await session.run(
        'MATCH (u:User {id: $user_id})-[r:IN_CART]->(v:Variant {id: $variant_id}) SET r.quantity = r.quantity + $quantity, r.created_at = $created_at',
        { user_id: user.id, variant_id: variant_id, quantity: quantity, created_at: dayjs().unix() }
      )
    } else {
      await session.run(
        'MATCH (u:User {id: $user_id}) MATCH (v:Variant {id: $variant_id}) MERGE (u)-[r:IN_CART {created_at: $created_at, quantity: $quantity}]->(v)',
        { user_id: user.id, variant_id: variant_id, created_at: dayjs().unix(), quantity: quantity }
      )
    }

    return res.json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const deleteFromCart = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = req.user
    const { variant_id } = req.query as IDeleteFromCart
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    await session.run('MATCH (u:User {id: $user_id})-[r:IN_CART]->(v:Variant {id: $variant_id}) DELETE r', {
      user_id: user.id,
      variant_id: parseInt(variant_id)
    })
    return res.json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const getFromWish = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = req.user
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }

    const data = await session.run(
      'MATCH (u:User {id: $id})-[r:IN_WISH]->(p:Product) RETURN p ORDER BY r.created_at DESC',
      { id: user.id }
    )

    const products_id = data.records.map((record) => record.get('p').properties.id)
    const products = await prisma.product.findMany({
      select: {
        id: true,
        name: true,
        product_image: {
          select: {
            image: true
          },
          take: 1
        },
        variants: {
          take: 1
        },
        category: {
          select: {
            parent_category: {
              select: {
                parent_category_id: true
              }
            }
          }
        }
      },
      where: {
        id: {
          in: products_id
        }
      }
    })
    return res.json(resultTemplate('SUCCESS', 'SUCCESS', products))
  } catch (error: any) {
    next(error)
  }
}

export const addToWish = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = req.user
    const { product_id } = req.body as IAddToWish
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    const existed = await session.run(
      'MATCH (u:User {id: $user_id})-[r:IN_WISH]->(p:Product {id: $product_id}) RETURN r',
      { user_id: user.id, product_id }
    )
    if (existed.records.length > 0) {
      throw new AppError('BAD_REQUEST', 'PRODUCT_ALREADY_EXISTED')
    } else {
      await session.run(
        'MATCH (u:User {id: $user_id}) MATCH (p:Product {id: $product_id}) MERGE (u)-[r:IN_WISH {created_at: $created_at}]->(p)',
        { user_id: user.id, product_id: product_id, created_at: dayjs().unix() }
      )
    }
    const category = await prisma.product.findUnique({
      select: {
        category: {
          select: {
            parent_category: {
              select: {
                parent_category_id: true
              }
            }
          }
        }
      },
      where: {
        id: product_id
      }
    })

    return res.json(resultTemplate('SUCCESS', 'SUCCESS', { category }))
  } catch (error: any) {
    next(error)
  }
}

export const deleteFromWish = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = req.user
    const { product_id } = req.query as IDeleteFromWish
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    await session.run('MATCH (u:User {id: $user_id})-[r:IN_WISH]->(p:Product {id: $product_id}) DELETE r', {
      user_id: user.id,
      product_id: parseInt(product_id)
    })
    return res.json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const getViewedProduct = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = req.user
    const query = req.query as IGetViewedProduct
    const page = query.page ? parseInt(query.page) : 1
    const limit = query.limit ? parseInt(query.limit) : 10
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    const data = await session.run(
      `MATCH (u:User {id: ${user.id}})-[r:VIEWED]->(p:Product) RETURN p ORDER BY r.created_at DESC SKIP ${(page - 1) * limit} LIMIT ${limit}`
    )
    const products_id = data.records.map((record) => record.get('p').properties.id)
    const result: any[] = []
    for (let i = 0; i < products_id.length; i++) {
      const product = await prisma.product.findUnique({
        select: {
          id: true,
          name: true,
          product_image: {
            select: {
              image: true,
              color_id: true
            },
            take: 1
          },
          variants: {
            take: 1
          }
        },
        where: {
          id: products_id[i]
        }
      })
      if (product) {
        result.push(product)
      }
    }
    return res.json(resultTemplate('SUCCESS', 'SUCCESS', result))
  } catch (error: any) {
    next(error)
  }
}

export const addViewedProduct = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { product_id } = req.body as IAddViewedProduct
    const user = req.user
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    const existed = await session.run(
      'MATCH (u:User {id: $user_id})-[r:VIEWED]->(p:Product {id: $product_id}) RETURN r',
      { user_id: user.id, product_id }
    )
    if (existed.records.length > 0) {
      await session.run(
        'MATCH (u:User {id: $user_id})-[r:VIEWED]->(p:Product {id: $product_id}) SET r.count = r.count + $count, r.created_at = $created_at',
        { user_id: user.id, product_id: product_id, count: 1, created_at: dayjs().unix() }
      )
    } else {
      await session.run(
        'MATCH (u:User {id: $user_id}) MATCH (p:Product {id: $product_id}) MERGE (u)-[r:VIEWED {created_at: $created_at, count: $count}]->(p)',
        { user_id: user.id, product_id: product_id, created_at: dayjs().unix(), count: 1 }
      )
    }
    return res.json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const getRecommendProductFromUser = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = req.user
    const query = req.query as IGetRecommendProductFromUser
    const page = query.page ? parseInt(query.page) : 1
    const limit = query.limit ? parseInt(query.limit) : 10
    const root_category_id = query.root_category_id ? parseInt(query.root_category_id) : undefined
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    const bought_cart_category = await session.run(`
      MATCH (u:User {id: ${user.id}})-[r]->(v:Variant)<-[:HAS]-(p)-[:IS_IN]->(c:Category)-[:HAS_PARENT]->()-[:HAS_PARENT]->(rc:Category)
      ${root_category_id ? `WHERE rc.id = ${root_category_id}` : ''}
      RETURN c.id, r.created_at
      ORDER BY r.created_at DESC LIMIT 10
      `)

    const viewed_wish_category = await session.run(`
      MATCH (u:User {id: ${user.id}}) -[r]-> (p:Product)-[:IS_IN]->(c:Category)-[:HAS_PARENT]->()-[:HAS_PARENT]->(rc:Category)
      ${root_category_id ? `WHERE rc.id = ${root_category_id}` : ''}
      RETURN c.id, r.created_at
      ORDER BY r.created_at DESC LIMIT 10
      `)

    const viewed_wish_categories_id: { id: number; created_at: number }[] = bought_cart_category.records.map(
      (record) => ({
        id: record.get('c.id'),
        created_at: record.get('r.created_at')
      })
    )
    const bought_cart_categories_id: { id: number; created_at: number }[] = viewed_wish_category.records.map(
      (record) => ({
        id: record.get('c.id'),
        created_at: record.get('r.created_at')
      })
    )

    // Merge two lists
    const mergedList = [...viewed_wish_categories_id, ...bought_cart_categories_id]

    // Sort by created_at
    mergedList.sort((a, b) => b.created_at - a.created_at)

    const filterList: { id: number; created_at: number }[] = []
    // Filter distinct items based on id with highest created_at
    for (let i = 0; i < mergedList.length; i++) {
      if (!filterList.find((item) => item.id === mergedList[i].id)) {
        filterList.push(mergedList[i])
      }
    }
    const categories_id = filterList.map((item) => item.id)
    const products: any[] = []
    for (let i = 0; i < categories_id.length; i++) {
      const productsData = await prisma.product.findMany({
        select: {
          id: true,
          name: true,
          product_image: {
            select: {
              image: true,
              color_id: true
            },
            take: 1
          },
          variants: {
            take: 1
          }
        },
        where: {
          category_id: categories_id[i]
        },
        skip: (page - 1) * limit,
        take: limit
      })
      products.push(...productsData)
    }

    return res.json(resultTemplate('SUCCESS', 'SUCCESS', products))
  } catch (error: any) {
    next(error)
  }
}

export const getRecommendProductFromOtherUser = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = req.user
    const query = req.query as IGetRecommendProductFromUser
    const page = query.page ? parseInt(query.page) : 1
    const limit = query.limit ? parseInt(query.limit) : 10
    const root_category_id = query.root_category_id ? parseInt(query.root_category_id) : undefined
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    // Mối quan hệ giữa cùng mua một sản phẩm  người kia mua một sản phẩm khác mà người kia chưa mua
    const bought_products_id_neo4j = await session.run(`
    MATCH (u1:User {id: ${user.id}})-[:BOUGHT]->(v:Variant)
    MATCH (v)<-[:HAS]-(p1:Product)
    MATCH (u_other:User)-[:BOUGHT]->()<-[:HAS]-(p1:Product)
    WHERE u_other.id <> ${user.id}
    WITH u_other, COLLECT(DISTINCT p1) AS products1
    MATCH (u_other)-[:BOUGHT]->(:Variant)<-[:HAS]-(p2:Product)${root_category_id ? `-[:IS_IN]->(c:Category)-[:HAS_PARENT]->()-[:HAS_PARENT]->(rc:Category {id:${root_category_id}})` : ''}
    WHERE NOT p2 IN products1
    RETURN p2.id
    LIMIT 10
    `)

    // Mối quan hệ giữa cùng mua một sản phẩm và trong giỏ hàng của người kia mà người kia chưa mua
    const cart_products_id_neo4j = await session.run(`
    MATCH (u1:User {id: ${user.id}})-[:BOUGHT]->(v:Variant)
    MATCH (v)<-[:HAS]-(p1:Product)
    MATCH (u_other:User)-[:BOUGHT]->()<-[:HAS]-(p1:Product)
    WHERE u_other.id <> ${user.id}
    WITH u_other, COLLECT(DISTINCT p1) AS products1
    MATCH (u_other)-[:IN_CART]->(:Variant)<-[:HAS]-(p2:Product)${root_category_id ? `-[:IS_IN]->(c:Category)-[:HAS_PARENT]->()-[:HAS_PARENT]->(rc:Category {id:${root_category_id}})` : ''}
    WHERE NOT p2 IN products1
    RETURN p2.id
    LIMIT 10
    `)

    // Mối quan hệ giữa cùng mua một sản phẩm và người còn lại đã xem một sản phẩm khác
    const viewed_products_id_neo4j = await session.run(`
    MATCH (u1:User {id: ${user.id}})-[:BOUGHT]->(v:Variant)
    MATCH (v)<-[:HAS]-(p1:Product)
    MATCH (u_other:User)-[:BOUGHT]->()<-[:HAS]-(p1:Product)
    WHERE u_other.id <> ${user.id}
    WITH u_other, COLLECT(DISTINCT p1) AS products1
    MATCH (u_other)-[:VIEWED]->(p2:Product)${root_category_id ? `-[:IS_IN]->(c:Category)-[:HAS_PARENT]->()-[:HAS_PARENT]->(rc:Category {id:${root_category_id}})` : ''}
    WHERE NOT p2 IN products1
    RETURN p2.id
    LIMIT 10
    `)
    const liked_products_id_neo4j = await session.run(`
    MATCH (u1:User {id: ${user.id}})-[:BOUGHT]->(v:Variant)
    MATCH (v)<-[:HAS]-(p1:Product)
    MATCH (u_other:User)-[:BOUGHT]->()<-[:HAS]-(p1:Product)
    WHERE u_other.id <> ${user.id}
    WITH u_other, COLLECT(DISTINCT p1) AS products1
    MATCH (u_other)-[:IN_WISH]->(p2:Product)${root_category_id ? `-[:IS_IN]->(c:Category)-[:HAS_PARENT]->()-[:HAS_PARENT]->(rc:Category {id:${root_category_id}})` : ''}
    WHERE NOT p2 IN products1
    RETURN p2.id
    LIMIT 10
    `)

    const bought_products_id: number[] = bought_products_id_neo4j.records.map((record) => record.get('p2.id'))
    const cart_products_id: number[] = cart_products_id_neo4j.records.map((record) => record.get('p2.id'))
    const viewed_products_id: number[] = viewed_products_id_neo4j.records.map((record) => record.get('p2.id'))
    const liked_products_id: number[] = liked_products_id_neo4j.records.map((record) => record.get('p2.id'))
    // Tạo một đối tượng để lưu trữ id sản phẩm và trọng số tương ứng
    const productWeights: { [id: number]: number } = {}

    // Hàm để thêm trọng số cho mỗi sản phẩm trong danh sách
    const addWeight = (products: number[], weight: number) => {
      products.forEach((id) => {
        if (productWeights[id]) {
          productWeights[id] += weight
        } else {
          productWeights[id] = weight
        }
      })
    }

    // Thêm trọng số cho mỗi danh sách sản phẩm
    addWeight(bought_products_id, 50)
    addWeight(cart_products_id, 20)
    addWeight(viewed_products_id, 10)
    addWeight(liked_products_id, 20)

    // Tạo một mảng chứa tất cả các id sản phẩm, không có trùng lặp
    const allProducts = Array.from(
      new Set([...bought_products_id, ...cart_products_id, ...viewed_products_id, ...liked_products_id])
    )

    // Sắp xếp mảng sản phẩm dựa trên trọng số
    const sortedProducts = allProducts.sort((a, b) => productWeights[b] - productWeights[a])
    const products: any[] = []
    // Bây giờ, sortedProducts chứa id sản phẩm được sắp xếp theo trọng số

    for (let i = 0; i < sortedProducts.length && i < limit; i++) {
      const products_other = await prisma.product.findFirst({
        select: {
          id: true,
          name: true,
          product_image: {
            select: {
              image: true,
              color_id: true
            },
            take: 1
          },
          variants: {
            take: 1
          }
        },
        where: {
          id: sortedProducts[i]
        }
      })
      if (products_other) {
        products.push(products_other)
      }
    }
    if (products.length < 10) {
      const top_bought_products_id = await prisma.analyze_product_monthly.groupBy({
        by: ['product_id'],
        _count: {
          product_id: true
        },
        where: {
          product_id: {
            notIn: sortedProducts
          }
        },
        orderBy: { _count: { product_id: 'desc' } }
      })
      for (let i = 0; i < top_bought_products_id.length; i++) {
        const products_other = await prisma.product.findFirst({
          select: {
            id: true,
            name: true,
            product_image: {
              select: {
                image: true,
                color_id: true
              },
              take: 1
            },
            variants: {
              take: 1
            }
          },
          where: {
            id: top_bought_products_id[i].product_id
          }
        })
        if (products_other) {
          products.push(products_other)
        }
      }
    }
    return res.json(resultTemplate('SUCCESS', 'SUCCESS', products))
  } catch (error: any) {
    next(error)
  }
}
