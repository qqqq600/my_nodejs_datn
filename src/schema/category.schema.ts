import { Static, Type } from '@sinclair/typebox'
import { nameSchema, numberSchema } from './common.schema'

export const createCategorySchema = Type.Object({
  name: nameSchema,
  parent_category_id: Type.Optional(numberSchema),
  child_categories_id: Type.Optional(Type.Array(numberSchema))
})

export type ICreateCategory = Static<typeof createCategorySchema>

export const updateCategorySchema = Type.Object({
  name: Type.Optional(nameSchema),
  category_id: numberSchema,
  parent_category_id: Type.Optional(Type.Union([numberSchema, Type.String({ pattern: 'null' })])),
  child_categories_id: Type.Optional(Type.Array(numberSchema))
})

export type IUpdateCategory = Static<typeof updateCategorySchema>

export const getCategorySchema = Type.Object({
  parent_category_id: Type.Optional(numberSchema),
  child_category_id: Type.Optional(numberSchema)
})

export type IGetCategory = Static<typeof getCategorySchema>

export const deleteCategorySchema = Type.Object({
  category_id: numberSchema
})

export type IDeleteCategory = Static<typeof deleteCategorySchema>
