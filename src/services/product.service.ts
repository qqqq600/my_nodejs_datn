import { AppError } from '@/config/appError'
import { SORT } from '@/config/constants'
import prisma from '@/libs/prisma'
import axios, { AxiosError } from 'axios'

export const uploadFilesImageService = async (imagesId: string[], locations: string[]) => {
  const response = await axios.post(`${process.env.FASTAPI_BACKEND_URL}/upload/`, {
    images_id: imagesId,
    locations: locations
  })
  return response
}

export const deleteFilesImageService = async (image_id: string) => {
  const response = await axios.delete(`${process.env.FASTAPI_BACKEND_URL}/delete/`, {
    params: { id: image_id }
  })
  return response
}

export const searchByImageBase64 = async (base64Image: string) => {
  const response = await axios.post(`${process.env.FASTAPI_BACKEND_URL}/search_base64/`, {
    image_base64: base64Image
  })
  const images_id = response.data.similar_images_id
  return images_id
}

interface IGetProductService {
  product_id?: number
  category_id?: number
  search?: string
  rating?: number
  page: number
  limit: number
  voucher_id?: number
  sort_by?: string
  image?: string
  min_price?: number
  max_price?: number
}

export const getProductService = async ({
  category_id,
  limit,
  page,
  product_id,
  voucher_id,
  rating,
  sort_by,
  search,
  image,
  min_price,
  max_price
}: IGetProductService) => {
  const product_images_id: number[] = []
  if (image) {
    const images_id: string[] = await searchByImageBase64(image)
    images_id.map((str) => product_images_id.push(parseInt(str)))
    if (product_images_id.length === 0) {
      return []
    }
  }
  const handleSortBy = async (orderBy?: any) => {
    const variants = await prisma.variant.findMany({
      include: {
        product: {
          select: {
            id: true,
            name: true,
            avg_rating: true,
            product_image: {
              select: {
                id: true,
                image: true,
                color_id: true
              },
              take: 1
            }
          }
        }
      },
      where: {
        product: {
          OR: [
            { category_id: category_id },
            { category: { parent_category_id: category_id } },
            { category: { parent_category: { parent_category_id: category_id } } }
          ],
          product_image: image
            ? {
                some: {
                  id: {
                    in: product_images_id
                  }
                }
              }
            : undefined,
          id: product_id,
          avg_rating: rating ? { gte: rating } : undefined,
          name: search ? { contains: search.toLowerCase(), mode: 'insensitive' } : undefined
        },
        OR: voucher_id
          ? [
              { product: { product_apply_voucher: { some: { voucher_id } } } },
              {
                product: {
                  category: {
                    category_apply_voucher: {
                      some: {
                        voucher_id
                      }
                    }
                  }
                }
              },
              { variant_apply_voucher: { some: { voucher_id } } }
            ]
          : undefined,
        AND: [
          {
            price_sale: min_price ? { gte: min_price } : undefined
          },
          {
            price_sale: max_price ? { lte: max_price } : undefined
          }
        ]
      },
      distinct: ['product_id'],
      take: limit,
      skip: (page - 1) * limit,
      orderBy
    })
    const products = variants.map((item) => ({ variants: [{ ...item, product: undefined }], ...item.product }))
    if (image && !orderBy) {
      products.sort((a, b) => {
        const aImageId = a.product_image[0]?.id ?? -1 // Nếu không có product_image hoặc id, đặt giá trị là -1
        const bImageId = b.product_image[0]?.id ?? -1 // Nếu không có product_image hoặc id, đặt giá trị là -1
        const aIndex = product_images_id.indexOf(aImageId)
        const bIndex = product_images_id.indexOf(bImageId)

        // Nếu id không tồn tại trong product_images_id, đặt giá trị là Infinity để sắp xếp nó ra cuối cùng
        return (aIndex === -1 ? Infinity : aIndex) - (bIndex === -1 ? Infinity : bIndex)
      })
    }
    return products
  }
  switch (sort_by) {
    case SORT[0]:
      return handleSortBy([{ product: { avg_rating: 'desc' } }])
    case SORT[1]:
      return handleSortBy([{ price_sale: 'asc' }])
    case SORT[2]:
      return handleSortBy([{ price_sale: 'desc' }])
    case SORT[3]:
      return handleSortBy([
        {
          product: {
            created_at: 'desc'
          }
        }
      ])

    case SORT[4]:
    // TODO: Cần thực hiện việc lấy dữ liệu product từ Analyze_Product_monthly lấy ra sum 3 tháng gần nhất tính sum của 3 tháng đó và sort theo sum
    // return { rating: 'desc' }
    case SORT[5]:
      return handleSortBy([{ percent_sale: 'desc' }, { remain_quantity: 'desc' }])
    default:
      return await handleSortBy()
  }
}
