import { ORDER_STATE } from '@/config/constants'
import prisma from '@/libs/prisma'

export const countOrderOveview = async () => {
  const order = await prisma.order.findMany({
    select: {
      state: true
    }
  })
  const countResultMap = new Map<string, number>()
  order.forEach((order) => {
    if (!countResultMap.has(order.state)) {
      countResultMap.set(order.state, 0)
    }
    countResultMap.set(order.state, countResultMap.get(order.state)! + 1)
  })
  const countResult: { state: string; count: number }[] = []

  countResultMap.forEach((count, state) => {
    countResult.push({ state, count })
  })
  return countResult
}

export const ValueOrderOveview = async () => {
  const order_detail = await prisma.order_detail.findMany({
    select: {
      price: true,
      order: {
        select: {
          state: true
        }
      }
    }
  })
  const valueResultMap = new Map<string, number>()
  order_detail.forEach((item) => {
    if (!valueResultMap.has(item.order.state)) {
      valueResultMap.set(item.order.state, 0)
    }
    valueResultMap.set(item.order.state, valueResultMap.get(item.order.state)! + item.price)
  })
  const valueResult: { state: string; value: number }[] = []

  valueResultMap.forEach((value, state) => {
    valueResult.push({ state, value })
  })
  return valueResult
}
