import { NextFunction, Response } from 'express'
import logger from '../libs/logger'

export const loggingRequest = (req: any, res: Response, next: NextFunction) => {
  const getIP =
    ((req.headers['X-Forwarded-For'] || req.headers['x-forwarded-for'] || '') as string).split(',')[0] ||
    req.client.remoteAddress
  const ip = (getIP.length < 15 && getIP) || getIP.slice(7) || req.ip

  res.on('finish', async () => {
    if (res.statusCode < 400) {
      logger.info(`${res.statusCode} ${ip} ${req.method} ${req.originalUrl}`)
    } else {
      logger.error(`${res.statusCode} ${ip} ${req.method} ${req.originalUrl}`)
    }
  })
  next()
}
