import express, { Express } from 'express'
import validate from '@/middlewares/validate.middleware'
import { validateAdmin } from '@/middlewares/admin.middlewate'
import { validateUser } from '@/middlewares/user.middleware'
import {
  createVoucher,
  deleteVoucher,
  getAllVoucher,
  getVoucherDetail,
  updateUserOwnVoucher,
  updateVoucherInfo,
  updateVoucherRange,
  userGetAllVoucher
} from '@/controllers/voucher.controller'
import {
  createVoucherSchema,
  deleteVoucherSchema,
  getVoucherDetailSchema,
  updateUserOwnVoucherSchema,
  updateVoucherInfoSchema,
  updateVoucherRangeSchema
} from '@/schema/voucher.schema'

const router = express.Router()

const VoucherRoute = (app: Express) => {
  router.get('/', getAllVoucher)
  router.post('/', validate({ schema: createVoucherSchema, part: 'body' }), createVoucher)
  router.get('/user', validateUser, userGetAllVoucher)
  router.delete('/', validate({ schema: deleteVoucherSchema, part: 'query' }), deleteVoucher)
  router.patch('/update-info', validate({ schema: updateVoucherInfoSchema, part: 'body' }), updateVoucherInfo)
  router.patch('/update-range', validate({ schema: updateVoucherRangeSchema, part: 'body' }), updateVoucherRange)
  router.patch('/user', validate({ schema: updateUserOwnVoucherSchema, part: 'body' }), updateUserOwnVoucher)
  router.get('/range-all', validate({ schema: getVoucherDetailSchema, part: 'query' }), getVoucherDetail)
  return app.use('/voucher', router)
}

export default VoucherRoute
