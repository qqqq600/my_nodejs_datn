import { AppError } from '@/config/appError'
import express from 'express'
import cors from 'cors'
import dotenv from 'dotenv'
import '@/neo4j/configDbNeo4j'
import UserRoute from '@/routes/user.route'
import ProductRoute from '@/routes/product.route'
import logger from '@/libs/logger'
import VoucherRoute from '@/routes/voucher.route'
import { appErrorHandler, errorHandler } from '@/config/errorHandler'
import CategoryRoute from '@/routes/category.route'
import { loggingRequest } from './config/loggingRequest'
import StaffRoute from './routes/staff.route'
import OrderRoute from '@/routes/order.route'
import ReviewRoute from '@/routes/review.route'
import { createServer } from 'http'
import { Server } from 'socket.io'
import FakeReviewRoute from './routes/fake-review.route'
import { exec } from 'child_process'
var bodyParser = require('body-parser')

dotenv.config()

const app = express()
const httpServer = createServer(app)
const port = process.env.PORT_BACKEND || 8080
const io = new Server(httpServer, {
  cors: {
    origin: '*',
    methods: ['GET', 'POST']
  }
})
io.on('connection', (socket) => {
  try {
    socket.on('joinOrderRoom', async (orderId) => {
      console.log('joinOrderRoom', orderId)
      socket.join(`order_${orderId}`)
    })
    socket.on('leaveOrderRoom', async (orderId) => {
      console.log('leaveOrderRoom', orderId)
      socket.leave(`order_${orderId}`)
    })
  } catch (error) {
    logger.error(error)
  }
})
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }))
app.use(bodyParser.json({ limit: '10mb' }))

app.use((req, res, next) => {
  req.io = io
  next()
})

app.use(loggingRequest)
CategoryRoute(app)
UserRoute(app)
ProductRoute(app)
VoucherRoute(app)
StaffRoute(app)
OrderRoute(app)
ReviewRoute(app)
FakeReviewRoute(app)

app.post('/deploy', (req, res, next) => {
  try {
    const body = req.body
    if (body.project.name !== 'fashion-fushion-nodejs') {
      throw new AppError('BAD_REQUEST', 'FAILED')
    }
    exec(
      'cd ~/fashion-fushion-nodejs && git stash && git pull && npm install && npm run prisma:migrate && npm run build && pm2 restart 0'
    )
    return res.json('OK')
  } catch (error) {
    next(error)
  }
})

httpServer.listen(port, () => {
  logger.info(`🚀 Server ready at http://localhost:${port}`)
  logger.info('Press CTRL-C to stop\n')
})

app.get('/', (req, res) => {
  res.json('From BKU with Love!')
})

app.use(appErrorHandler)
app.use(errorHandler)
