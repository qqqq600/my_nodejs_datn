import axios from 'axios'

type GetUserInfoGoogle = {
  id: string
  email: string
  verified_email: boolean
  name: string
  given_name: string
  family_name: string
  picture: string
  locale: string
}

type GetUserInfoGoogleError = {
  error: {
    code: number
    message: string
    status: string
  }
}

export const getUserInfoGoogle = async (googleToken: string) => {
  try {
    const res = await axios({
      method: 'get',
      url: 'https://www.googleapis.com/userinfo/v2/me',
      headers: { Authorization: `Bearer ${googleToken}` }
    })
    return res.data as GetUserInfoGoogle
  } catch (error: any) {
    return error.response.data as GetUserInfoGoogleError
  }
}
