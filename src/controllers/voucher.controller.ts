import { NextFunction, Request, Response } from 'express'
import { AppError } from '@/config/appError'
import { resultTemplate } from '@/utils/result-template'
import prisma from '@/libs/prisma'
import {
  ICreateVoucher,
  ICreateVoucherVariant,
  IDeleteVoucher,
  IGetVoucherDetail,
  IUpdateUserOwnVoucher,
  IUpdateVoucherInfo,
  IUpdateVoucherRange
} from '@/schema/voucher.schema'
import dayjs from 'dayjs'
import { VOUCHER_TYPE } from '@/config/constants'

export const getAllVoucher = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const vouchers = await prisma.voucher.findMany()
    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', vouchers))
  } catch (error: any) {
    next(error)
  }
}

export const createVoucher = async (req: Request, res: Response, next: NextFunction) => {
  const createInfo: ICreateVoucher = req.body
  const dayjsStartedAt = dayjs.unix(createInfo.started_at)
  const dayjsExpiredAt = dayjs.unix(createInfo.expired_at)

  try {
    const now = dayjs()
    if (now.isAfter(dayjsExpiredAt) || dayjsStartedAt.isAfter(dayjsExpiredAt)) {
      throw new AppError('BAD_REQUEST', 'ERROR_APPLICATION_TIME')
    }
    if (createInfo.min_price_apply <= createInfo.max_price) {
      throw new AppError('BAD_REQUEST', 'ERROR_PRICE')
    }
    if (!(createInfo.type in VOUCHER_TYPE)) {
      throw new AppError('BAD_REQUEST', 'INVALID_VOUCHER_TYPE')
    }

    if (
      ('variants_id' in createInfo && createInfo.type != VOUCHER_TYPE.VARIANT) ||
      ('products_id' in createInfo && createInfo.type != VOUCHER_TYPE.PRODUCT) ||
      ('categories_id' in createInfo && createInfo.type != VOUCHER_TYPE.CATEGORY) ||
      (('variants_id' in createInfo || 'products_id' in createInfo || 'categories_id' in createInfo) &&
        createInfo.type == VOUCHER_TYPE.ALL)
    ) {
      throw new AppError('BAD_REQUEST', 'RANGE_LIST_AND_TYPE_UNLIKE')
    }

    // Check if the list of products passed is valid
    if ('categories_id' in createInfo) {
      const categories = await prisma.category.findMany({
        where: {
          id: {
            in: createInfo.categories_id
          }
        }
      })

      if (categories.length != createInfo.categories_id.length) {
        throw new AppError('BAD_REQUEST', 'INVALID_CATEGORY_LIST')
      }
    } else if ('products_id' in createInfo) {
      const products = await prisma.product.findMany({
        where: {
          id: {
            in: createInfo.products_id
          }
        }
      })

      if (products.length != createInfo.products_id.length) {
        throw new AppError('BAD_REQUEST', 'INVALID_PRODUCT_LIST')
      }
    } else if ('variants_id' in createInfo) {
      const variants = await prisma.variant.findMany({
        where: {
          id: {
            in: createInfo.variants_id
          }
        }
      })

      if (variants.length != createInfo.variants_id.length) {
        throw new AppError('BAD_REQUEST', 'INVALID_VARIANT_LIST')
      }
    }

    const newVoucher = await prisma.voucher.create({
      data: {
        name: createInfo.name,
        expired_at: dayjsExpiredAt.toDate(),
        max_price: createInfo.max_price,
        min_price_apply: createInfo.min_price_apply,
        started_at: dayjsStartedAt.toDate(),
        percent_discount: createInfo.percent_discount,
        remaining_quantity: createInfo.remaining_quantity,
        type: createInfo.type
      }
    })

    if ('categories_id' in createInfo) {
      await prisma.category_apply_voucher.createMany({
        data: createInfo.categories_id.map((category_id) => ({
          category_id: category_id,
          voucher_id: newVoucher.id
        }))
      })
    } else if ('products_id' in createInfo) {
      await prisma.product_apply_voucher.createMany({
        data: createInfo.products_id.map((product_id) => ({
          product_id: product_id,
          voucher_id: newVoucher.id
        }))
      })
    } else if ('variants_id' in createInfo) {
      await prisma.variant_apply_voucher.createMany({
        data: createInfo.variants_id.map((variant_id) => ({
          variant_id: variant_id,
          voucher_id: newVoucher.id
        }))
      })
    }
    if (createInfo.users_id) {
      await prisma.user_own_voucher.createMany({
        data: createInfo.users_id.map((user_id) => ({
          user_id: user_id,
          voucher_id: newVoucher.id
        }))
      })
    } else {
      const allUser = await prisma.user.findMany()
      await prisma.user_own_voucher.createMany({
        data: allUser.map((user) => ({
          user_id: user.id,
          voucher_id: newVoucher.id
        }))
      })
    }
    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', newVoucher))
  } catch (error: any) {
    next(error)
  }
}

export const userGetAllVoucher = async (req: Request, res: Response, next: NextFunction) => {
  const user = req.user
  const now = dayjs()

  try {
    const voucher = await prisma.voucher.findMany({
      where: {
        user_own_voucher: {
          some: {
            user_id: user?.id,
            status: false
          }
        },
        started_at: {
          lte: now.toDate()
        },
        expired_at: {
          gte: now.toDate()
        },
        remaining_quantity: {
          gt: 0
        }
      },
      include: {
        category_apply_voucher: true,
        product_apply_voucher: true,
        variant_apply_voucher: true
      }
    })
    const result = voucher.map((voucher) => {
      return {
        id: voucher.id,
        expired_at: voucher.expired_at,
        max_price: voucher.max_price,
        min_price_apply: voucher.min_price_apply,
        name: voucher.name,
        remaining_quantity: voucher.remaining_quantity,
        used_quantity: voucher.used_quantity,
        percent_discount: voucher.percent_discount,
        type: voucher.type,
        categories_id: voucher.category_apply_voucher.map((voucher) => voucher.category_id),
        products_id: voucher.product_apply_voucher.map((voucher) => voucher.product_id),
        variants_id: voucher.variant_apply_voucher.map((voucher) => voucher.variant_id)
      }
    })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', result))
  } catch (error: any) {
    next(error)
  }
}

export const deleteVoucher = async (req: Request, res: Response, next: NextFunction) => {
  const { voucher_id } = req.query as IDeleteVoucher
  try {
    const voucher = await prisma.voucher.findFirst({
      where: {
        id: parseInt(voucher_id)
      },
      include: {
        category_apply_voucher: true,
        product_apply_voucher: true,
        variant_apply_voucher: true
      }
    })
    if (!voucher) {
      throw new AppError('BAD_REQUEST', 'VOUCHER_NOT_FOUND')
    }
    if (voucher.type == VOUCHER_TYPE.CATEGORY) {
      await prisma.category_apply_voucher.deleteMany({
        where: {
          voucher_id: parseInt(voucher_id)
        }
      })
    } else if (voucher.type == VOUCHER_TYPE.PRODUCT) {
      await prisma.product_apply_voucher.deleteMany({
        where: {
          voucher_id: parseInt(voucher_id)
        }
      })
    } else if (voucher.type == VOUCHER_TYPE.VARIANT) {
      await prisma.variant_apply_voucher.deleteMany({
        where: {
          voucher_id: parseInt(voucher_id)
        }
      })
    }

    await prisma.order.updateMany({
      where: {
        voucher_id: parseInt(voucher_id)
      },
      data: {
        voucher_id: null
      }
    })
    await prisma.user_own_voucher.deleteMany({
      where: {
        voucher_id: parseInt(voucher_id)
      }
    })
    await prisma.voucher.delete({
      where: {
        id: parseInt(voucher_id)
      }
    })
    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const updateVoucherInfo = async (req: Request, res: Response, next: NextFunction) => {
  const newInfo: IUpdateVoucherInfo = req.body
  const dayjsStartedAt = dayjs.unix(newInfo.started_at)
  const dayjsExpiredAt = dayjs.unix(newInfo.expired_at)
  try {
    const voucher = await prisma.voucher.findFirst({
      where: {
        id: newInfo.voucher_id
      }
    })
    if (!voucher) {
      throw new AppError('BAD_REQUEST', 'VOUCHER_NOT_FOUND')
    }
    const now = dayjs()
    if (!now.isBefore(dayjsExpiredAt) && !dayjsStartedAt.isBefore(dayjsExpiredAt)) {
      throw new AppError('BAD_REQUEST', 'ERROR_APPLICATION_TIME')
    }
    if (newInfo.min_price_apply <= newInfo.max_price) {
      throw new AppError('BAD_REQUEST', 'ERROR_PRICE')
    }

    await prisma.voucher.update({
      where: {
        id: newInfo.voucher_id
      },
      data: {
        max_price: newInfo.max_price,
        name: newInfo.name,
        started_at: dayjsStartedAt.toDate(),
        expired_at: dayjsExpiredAt.toDate(),
        remaining_quantity: newInfo.remaining_quantity,
        min_price_apply: newInfo.min_price_apply,
        percent_discount: newInfo.percent_discount
      }
    })
    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const updateVoucherRange = async (req: Request, res: Response, next: NextFunction) => {
  const newVoucherRange = req.body as IUpdateVoucherRange
  try {
    const voucher = await prisma.voucher.findFirst({
      where: {
        id: newVoucherRange.voucher_id
      }
    })

    if (!voucher) {
      throw new AppError('BAD_REQUEST', 'VOUCHER_NOT_FOUND')
    }
    if (!(newVoucherRange.type in VOUCHER_TYPE)) {
      throw new AppError('BAD_REQUEST', 'INVALID_VOUCHER_TYPE')
    }
    if (
      ('variants_id' in newVoucherRange && newVoucherRange.type != VOUCHER_TYPE.VARIANT) ||
      ('products_id' in newVoucherRange && newVoucherRange.type != VOUCHER_TYPE.PRODUCT) ||
      ('categories_id' in newVoucherRange && newVoucherRange.type != VOUCHER_TYPE.CATEGORY) ||
      (('variants_id' in newVoucherRange || 'products_id' in newVoucherRange || 'categories_id' in newVoucherRange) &&
        newVoucherRange.type == VOUCHER_TYPE.ALL)
    ) {
      throw new AppError('BAD_REQUEST', 'RANGE_LIST_AND_TYPE_UNLIKE')
    }

    // Check if the list of products passed is valid
    if ('categories_id' in newVoucherRange) {
      const categories = await prisma.category.findMany({
        where: {
          id: {
            in: newVoucherRange.categories_id
          }
        }
      })

      if (categories.length != newVoucherRange.categories_id.length) {
        throw new AppError('BAD_REQUEST', 'INVALID_CATEGORY_LIST')
      }
    } else if ('products_id' in newVoucherRange) {
      const products = await prisma.product.findMany({
        where: {
          id: {
            in: newVoucherRange.products_id
          }
        }
      })

      if (products.length != newVoucherRange.products_id.length) {
        throw new AppError('BAD_REQUEST', 'INVALID_PRODUCT_LIST')
      }
    } else if ('variants_id' in newVoucherRange) {
      const variants = await prisma.variant.findMany({
        where: {
          id: {
            in: newVoucherRange.variants_id
          }
        }
      })

      if (variants.length != newVoucherRange.variants_id.length) {
        throw new AppError('BAD_REQUEST', 'INVALID_VARIANT_LIST')
      }
    }


    // dalete data range
    if (voucher.type == VOUCHER_TYPE.CATEGORY) {
      await prisma.category_apply_voucher.deleteMany({
        where: {
          voucher_id: newVoucherRange.voucher_id
        }
      })
    } else if (voucher.type == VOUCHER_TYPE.PRODUCT) {
      await prisma.product_apply_voucher.deleteMany({
        where: {
          voucher_id: newVoucherRange.voucher_id
        }
      })
    } else if (voucher.type == VOUCHER_TYPE.VARIANT) {
      await prisma.variant_apply_voucher.deleteMany({
        where: {
          voucher_id: newVoucherRange.voucher_id
        }
      })
    }

    //update type
    if (voucher.type != newVoucherRange.type) {
      await prisma.voucher.update({
        where: {
          id: newVoucherRange.voucher_id
        },
        data: {
          type: newVoucherRange.type
        }
      })
    }

    // create new range data
    if ('variants_id' in newVoucherRange) {
      await prisma.variant_apply_voucher.createMany({
        data: newVoucherRange.variants_id.map((variant_id) => ({
          variant_id: variant_id,
          voucher_id: newVoucherRange.voucher_id
        }))
      })
    } else if ('products_id' in newVoucherRange) {
      await prisma.product_apply_voucher.createMany({
        data: newVoucherRange.products_id.map((products_id) => ({
          product_id: products_id,
          voucher_id: newVoucherRange.voucher_id
        }))
      })
    } else if ('categories_id' in newVoucherRange) {
      await prisma.category_apply_voucher.createMany({
        data: newVoucherRange.categories_id.map((category_id) => ({
          category_id: category_id,
          voucher_id: newVoucherRange.voucher_id
        }))
      })
    }
    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const updateUserOwnVoucher = async (req: Request, res: Response, next: NextFunction) => {
  const newInfo: IUpdateUserOwnVoucher = req.body
  try {
    const voucher = await prisma.voucher.findFirst({
      where: {
        id: newInfo.voucher_id
      }
    })
    if (!voucher) {
      throw new AppError('BAD_REQUEST', 'VOUCHER_NOT_FOUND')
    }
    await prisma.user_own_voucher.deleteMany({
      where: {
        voucher_id: newInfo.voucher_id
      }
    })
    if (newInfo.users_id) {
      await prisma.user_own_voucher.createMany({
        data: newInfo.users_id.map((user_id) => ({
          user_id: user_id,
          voucher_id: newInfo.voucher_id
        }))
      })
    } else {
      const allUser = await prisma.user.findMany()
      await prisma.user_own_voucher.createMany({
        data: allUser.map((user) => ({
          user_id: user.id,
          voucher_id: newInfo.voucher_id
        }))
      })
    }
    res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const getVoucherDetail = async (req: Request, res: Response, next: NextFunction) => {
  const { voucher_id } = req.query as IGetVoucherDetail
  try {
    const voucherInfo = await prisma.voucher.findFirst({
      where: {
        id: parseInt(voucher_id)
      }
    })
    if (!voucherInfo) {
      throw new AppError('BAD_REQUEST', 'VOUCHER_NOT_FOUND')
    }
    const user_own_voucher = await prisma.user_own_voucher.findMany({
      where: {
        voucher_id: parseInt(voucher_id)
      },
      include: {
        user: {
          select: {
            id: true,
            email: true,
            name: true,
            image: true
          }
        }
      }
    })
    if (voucherInfo.type == VOUCHER_TYPE.CATEGORY) {
      const categoryLists = await prisma.category_apply_voucher.findMany({
        where: {
          voucher_id: voucherInfo.id
        },
        select: {
          id: true,
          voucher: {
            select: {
              id: true,
              type: true
            }
          },
          category: {
            select: {
              id: true,
              name: true,
              image: true
            }
          }
        }
      })
      res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', { voucherInfo, categoryLists, user_own_voucher }))
    } else if (voucherInfo.type == VOUCHER_TYPE.PRODUCT) {
      const productLists = await prisma.product_apply_voucher.findMany({
        where: {
          voucher_id: voucherInfo.id
        },
        select: {
          id: true,
          voucher: {
            select: {
              id: true,
              type: true
            }
          },
          product: {
            select: {
              id: true,
              name: true
            }
          }
        }
      })
      res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', { voucherInfo, productLists, user_own_voucher }))
    } else if (voucherInfo.type == VOUCHER_TYPE.VARIANT) {
      const variantLists = await prisma.variant_apply_voucher.findMany({
        where: {
          voucher_id: voucherInfo.id
        },
        select: {
          id: true,
          voucher_id: true,
          variant: {
            select: {
              id: true,
              size: true,
              color: true,
              product: {
                select: {
                  name: true,
                  id: true
                }
              }
            }
          }
        }
      })
      res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', { voucherInfo, variantLists, user_own_voucher }))
    } else {
      const allLists = await prisma.product.findMany({
        select: {
          id: true,
          name: true
        }
      })
      res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', { voucherInfo, allLists, user_own_voucher }))
    }
  } catch (error: any) {
    next(error)
  }
}
