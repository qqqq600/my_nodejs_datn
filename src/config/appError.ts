import { CODE, CodeKeyType, MESSAGE, MessageKeyType } from './constants'

export class AppError extends Error {
  public statusCode: CodeKeyType
  constructor(code: CodeKeyType, message: MessageKeyType, data?: any) {
    super(message)
    this.statusCode = code
    Object.setPrototypeOf(this, AppError.prototype)
  }
}
