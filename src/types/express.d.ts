import { Prisma } from '@prisma/client'

export type RequestUser = Prisma.UserGetPayload<{
  select: {
    id: true
    name: true
    email: true
    phone_number: true
    image: true
    sex: true
    date_of_birth: true
    last_login_at: true
    registry_at: true
  }
}>

export type RequestAdmin = Prisma.StaffGetPayload<{
  select: {
    id: true
    name: true
    email: true
    phone_number: true
    image: true
    last_login_at: true
    registry_at: true
    role: true
  }
}>

declare global {
  declare namespace Express {
    interface Request {
      user?: RequestUser
      admin?: RequestAdmin
      io?: Server<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap, any>
    }
  }
}
