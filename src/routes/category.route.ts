import express, { Express } from 'express'
import * as categoryController from '../controllers/category.controller'
import validate from '@/middlewares/validate.middleware'
import {
  createCategorySchema,
  deleteCategorySchema,
  getCategorySchema,
  updateCategorySchema
} from '@/schema/category.schema'
import { uploadImage } from '@/services/s3.service'
const router = express.Router()

const CategoryRoute = (app: Express) => {
  router.get('/', validate({ schema: getCategorySchema, part: 'query' }), categoryController.getCategory)
  router.get('/leaf', categoryController.getCategories)
  router.post(
    '/',
    uploadImage('category').single('image'),
    validate({ schema: createCategorySchema, part: 'body' }),
    categoryController.addCategory
  )
  router.patch(
    '/',
    uploadImage('category').single('image'),
    validate({ schema: updateCategorySchema, part: 'body' }),
    categoryController.updateCategory
  )
  router.delete('/', validate({ schema: deleteCategorySchema, part: 'query' }), categoryController.deleteCategory)

  return app.use('/category', router)
}

export default CategoryRoute
