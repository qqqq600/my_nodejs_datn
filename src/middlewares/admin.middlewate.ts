import { AppError } from '@/config/appError'
import { NextFunction, Request, Response } from 'express'
import prisma from '@/libs/prisma'
import { RequestUser } from '@/types/express'
import { parseJwt, validateJSONToken } from '@/utils/jwt'

export const validateAdmin = async (req: Request, res: Response, next: NextFunction) => {
  try {
    if (req.method === 'OPTIONS') {
      return next()
    }

    if (!req.headers.authorization) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }

    const authFragments = req.headers.authorization.split(' ')

    if (authFragments.length !== 2) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }

    const authToken = authFragments[1]
    const validatedToken = validateJSONToken(authToken)
    const id_user = parseJwt(authFragments[1]).id
    let admin = await prisma.staff.findFirst({ where: { id: id_user } })
    if (!admin) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    req.admin = admin
    next()
  } catch (error: any) {
    next(error)
  }
}
