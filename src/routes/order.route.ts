import express, { Express } from 'express'
import validate from '@/middlewares/validate.middleware'
import { validateAdmin } from '@/middlewares/admin.middlewate'
import { validateUser } from '@/middlewares/user.middleware'
import {
  createOrder,
  getOrderDetail,
  getOrderOfUser,
  payosTransferHandler,
  updateOrderStateAdmin,
  userCancelOrder,
  orderDetailOfUserNotYetReview,
  getOrderAdmin,
  updatePaymentMethod
} from '@/controllers/order.controller'
import {
  createOrderSchema,
  getOrderAdminSchema,
  getOrderDetailSchema,
  getOrderOfUserSchema,
  orderDetailOfUserNotYetReviewSchema,
  updateOrderStateSchema,
  updatePaymentMethodSchema,
  userCancelOrderSchema
} from '@/schema/order.schema'

const router = express.Router()

const OrderRoute = (app: Express) => {
  router.post('/payment/transfer', payosTransferHandler)

  router.post('/create', validateUser, validate({ schema: createOrderSchema, part: 'body' }), createOrder)
  router.get('/user', validateUser, validate({ schema: getOrderOfUserSchema, part: 'query' }), getOrderOfUser)

  router.get('/detail', validate({ schema: getOrderDetailSchema, part: 'query' }), getOrderDetail)
  router.patch(
    '/user/cancel',
    validateUser,
    validate({ schema: userCancelOrderSchema, part: 'query' }),
    userCancelOrder
  )

  router.patch(
    '/user/payment',
    validateUser,
    validate({ schema: updatePaymentMethodSchema, part: 'body' }),
    updatePaymentMethod
  )

  router.get('/admin', validateAdmin, validate({ schema: getOrderAdminSchema, part: 'query' }), getOrderAdmin)

  router.patch(
    '/admin/state',
    validateAdmin,
    validate({ schema: updateOrderStateSchema, part: 'query' }),
    updateOrderStateAdmin
  )
  router.get(
    '/not-review',
    validateUser,
    validate({ schema: orderDetailOfUserNotYetReviewSchema, part: 'query' }),
    orderDetailOfUserNotYetReview
  )
  return app.use('/order', router)
}

export default OrderRoute
