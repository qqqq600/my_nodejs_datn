import express, { Express } from 'express'
import validate from '@/middlewares/validate.middleware'
import { validateAdmin } from '@/middlewares/admin.middlewate'
import {
  createStaffSchema,
  getBestSellingSchema,
  getRevenueSchema,
  getStaffDetailSchema,
  staffLoginSchema
} from '@/schema/staff.schema'
import {
  createStaff,
  deleteUser,
  getAllStaff,
  getBestSelling,
  getOverView,
  getRevenue,
  getStaffDetail,
  staffLogin
} from '@/controllers/staff.controller'
import { deleteUserSchema } from '@/schema/staff.schema'

const router = express.Router()

const StaffRoute = (app: Express) => {
  router.post('/login', validate({ schema: staffLoginSchema, part: 'body' }), staffLogin)
  router.use(validateAdmin)
  router.post('/create', validate({ schema: createStaffSchema, part: 'body' }), createStaff)
  router.delete('/user', validate({ schema: deleteUserSchema, part: 'query' }), deleteUser)
  router.get('/', getAllStaff)
  router.get('/detail', validate({ schema: getStaffDetailSchema, part: 'query' }), getStaffDetail)
  router.get('/over-view', getOverView)
  router.get('/revenue', validate({ schema: getRevenueSchema, part: 'query' }), getRevenue)
  router.get('/best-selling', validate({ schema: getBestSellingSchema, part: 'query' }), getBestSelling)
  return app.use('/staff', router)
}

export default StaffRoute
