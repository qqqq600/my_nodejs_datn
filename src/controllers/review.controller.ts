import { Request, Response, NextFunction } from 'express'
import { badWords, blackList } from 'vn-badwords'
import { AppError } from '@/config/appError'
import prisma from '@/libs/prisma'
import { resultTemplate } from '@/utils/result-template'
import { IAdminUpdateReview, ICreateReview, IDeleteReview, IGetReview, IGetReviewOfUser } from '@/schema/review.schema'

import { ORDER_STATE } from '@/config/constants'
import { updateRatingInfor, validateContentReview } from '@/services/review.service'
import { deleteImage } from '@/services/s3.service'

export const getReview = async (req: Request, res: Response, next: NextFunction) => {
  const { product_id, user_id, limit, page, is_disable } = req.query as IGetReview
  try {
    const reviews = await prisma.review.findMany({
      where: {
        order_detail: {
          variant: {
            product: {
              id: product_id ? parseInt(product_id) : undefined
            }
          }
        },
        is_disable: is_disable === 'true' ? true : is_disable === 'false' ? false : undefined,
        user_id: user_id ? parseInt(user_id) : undefined
      },
      select: {
        content: true,
        created_at: true,
        id: true,
        image: true,
        is_disable: true,
        rating: true,
        user: {
          select: {
            id: true,
            email: true,
            phone_number: true,
            image: true,
            name: true
          }
        }
      },
      take: limit ? parseInt(limit) : undefined,
      skip: page ? (parseInt(page) - 1) * (limit ? parseInt(limit) : 10) : undefined
    })
    if (product_id) {
      const ratingCountsByProduct = await prisma.review.groupBy({
        by: ['rating'],
        _count: {
          rating: true
        },
        where: {
          is_disable: is_disable === 'true' ? true : is_disable === 'false' ? false : undefined,
          order_detail: {
            variant: {
              product: {
                id: parseInt(product_id)
              }
            }
          }
        }
      })
      return res.json(resultTemplate('SUCCESS', 'SUCCESS', { reviews, ratingCounts: ratingCountsByProduct }))
    }

    return res.json(resultTemplate('SUCCESS', 'SUCCESS', { reviews }))
  } catch (error: any) {
    next(error)
  }
}

export const getReviewOfUser = async (req: Request, res: Response, next: NextFunction) => {
  const { limit, page } = req.query as IGetReviewOfUser
  const user = req?.user
  try {
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    if (!user) {
      throw new AppError('UNAUTHORIZED', 'UNAUTHORIZED')
    }
    const reviews = await prisma.review.findMany({
      where: {
        user_id: user.id
      },
      select: {
        id: true,
        content: true,
        rating: true,
        is_disable: true,
        created_at: true,
        image: true,
        user: {
          select: {
            id: true,
            email: true,
            phone_number: true,
            image: true,
            name: true
          }
        },
        order_detail: {
          select: {
            variant: {
              select: {
                product: {
                  select: {
                    id: true,
                    name: true,
                    product_image: {
                      take: 1
                    }
                  }
                }
              }
            }
          }
        }
      },
      take: limit ? parseInt(limit) : undefined,
      skip: page ? (parseInt(page) - 1) * (limit ? parseInt(limit) : 10) : undefined
    })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', reviews))
  } catch (error: any) {
    next(error)
  }
}

export const createReview = async (req: Request, res: Response, next: NextFunction) => {
  const reviewInfo = req.body as ICreateReview
  const content = reviewInfo.content
  const order_detail_id = parseInt(reviewInfo.order_detail_id)
  const rating = parseInt(reviewInfo.rating)
  const user = req.user
  const files = req.files as Express.MulterS3.File[]
  try {
    if (!user) {
      throw new AppError('BAD_REQUEST', 'ACCOUNT_NOT_FOUND')
    }

    const orderDetail = await prisma.order_detail.findFirst({
      where: {
        id: order_detail_id,
        order: {
          recipient_information: {
            user_id: user?.id
          }
        }
      },
      include: {
        order: {
          select: {
            state: true
          }
        }
      }
    })

    if (!orderDetail) {
      throw new AppError('BAD_REQUEST', 'THIS_IS_NOT_YOURS')
    }
    if (orderDetail.order.state != ORDER_STATE.DONE) {
      throw new AppError('BAD_REQUEST', 'ORDER_NOT_COMPLETED_YET')
    }

    const checkReview = await prisma.review.findFirst({
      where: {
        order_detail_id: order_detail_id,
        user_id: user.id
      }
    })
    if (checkReview) {
      throw new AppError('BAD_REQUEST', 'REVIEW_EXISTED')
    }
    const review = await prisma.review.create({
      data: {
        content: content || null,
        rating: rating,
        is_disable: false,
        order_detail_id: order_detail_id,
        user_id: user.id,
        image: files.length > 0 ? files[0].location : null
      }
    })

    const reviewOtherThread = async () => {
      try {
        let is_disable: boolean | string = false
        if (content) {
          is_disable = await validateContentReview(content)
          if (content) {
            is_disable = await validateContentReview(content)
          }
          if (is_disable as Boolean) {
            await prisma.review.update({
              where: {
                id: review.id
              },
              data: {
                is_disable: true
              }
            })
          }
          //update avg_Rating in product
          const product = await prisma.product.findFirst({
            where: {
              variants: {
                some: {
                  order_detail: {
                    some: {
                      review: {
                        some: {
                          id: review.id
                        }
                      }
                    }
                  }
                }
              }
            }
          })
          if (product && !review.is_disable) {
            await updateRatingInfor(product.id)
          }
        }
      } catch (error) {
        console.log('error', error)
      }
    }

    reviewOtherThread()
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const deleteReview = async (req: Request, res: Response, next: NextFunction) => {
  const { review_id } = req.query as IDeleteReview
  try {
    const review = await prisma.review.findFirst({
      where: {
        id: parseInt(review_id)
      }
    })
    if (!review) {
      throw new AppError('BAD_REQUEST', 'REVIEW_NOT_FOUND')
    }
    //update avg_Rating in product
    const product = await prisma.product.findFirst({
      where: {
        variants: {
          some: {
            order_detail: {
              some: {
                review: {
                  some: {
                    id: review.id
                  }
                }
              }
            }
          }
        }
      }
    })

    const review_deleted = await prisma.review.delete({
      where: {
        id: review.id
      }
    })
    if (review_deleted.image) {
      await deleteImage(review_deleted.image)
    }
    if (product) {
      await updateRatingInfor(product.id)
    }

    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const adminUpdateReview = async (req: Request, res: Response, next: NextFunction) => {
  const { review_id, is_disable } = req.body as IAdminUpdateReview
  try {
    const review = await prisma.review.findFirst({
      where: {
        id: review_id
      }
    })
    if (!review) {
      throw new AppError('BAD_REQUEST', 'REVIEW_NOT_FOUND')
    }
    if (review.is_disable != is_disable) {
      const new_review = await prisma.review.update({
        where: {
          id: review_id
        },
        data: {
          is_disable: is_disable
        },
        include: {
          order_detail: {
            select: {
              variant: {
                select: {
                  product_id: true
                }
              }
            }
          }
        }
      })

      updateRatingInfor(new_review.order_detail.variant.product_id)

      return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', new_review))
    }
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', review))
  } catch (error: any) {
    next(error)
  }
}
