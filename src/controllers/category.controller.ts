import { AppError } from '@/config/appError'
import { ICreateCategory, IDeleteCategory, IGetCategory, IUpdateCategory } from '@/schema/category.schema'
import { resultTemplate } from '@/utils/result-template'
import { NextFunction, Request, Response } from 'express'
import prisma from '@/libs/prisma'
import { deleteImage } from '@/services/s3.service'
import { session } from '@/neo4j/configDbNeo4j'

export const getCategory = async (req: Request, res: Response, next: NextFunction) => {
  const { child_category_id, parent_category_id }: IGetCategory = req.query
  try {
    if (child_category_id && parent_category_id) {
      throw new AppError('BAD_REQUEST', 'INVALID_PARAMS')
    }
    if (child_category_id) {
      // get category with child_category_id
      const category = await prisma.category.findFirst({
        where: {
          child_categories: {
            some: {
              id: parseInt(child_category_id)
            }
          }
        }
      })
      return res.json(resultTemplate('SUCCESS', 'SUCCESS', { ...category }))
    } else if (parent_category_id) {
      // get category with parent_category_id
      const category = await prisma.category.findMany({
        where: {
          parent_category_id: parseInt(parent_category_id)
        }
      })
      return res.json(resultTemplate('SUCCESS', 'SUCCESS', category))
    } else {
      // get All category
      const category = await prisma.category.findMany({
        select: {
          id: true,
          name: true,
          image: true,
          child_categories: {
            select: {
              id: true,
              name: true,
              image: true,
              child_categories: {
                select: {
                  id: true,
                  name: true,
                  image: true
                }
              }
            }
          }
        },
        where: {
          parent_category_id: null
        },
        orderBy: { id: 'asc' }
      })
      return res.json(resultTemplate('SUCCESS', 'SUCCESS', category))
    }
  } catch (error: any) {
    next(error)
  }
}

export const getCategories = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const categories = await prisma.category.findMany({
      where: {
        child_categories: {
          none: {}
        }
      }
    })
    return res.json(resultTemplate('SUCCESS', 'SUCCESS', categories))
  } catch (error: any) {
    next(error)
  }
}

export const addCategory = async (req: Request, res: Response, next: NextFunction) => {
  const { child_categories_id, name }: ICreateCategory = req.body
  const parent_category_id = req.body.parent_category_id ? parseInt(req.body.parent_category_id) : null

  if (parent_category_id) {
    const cate = await prisma.category.findFirst({
      where: {
        parent_category: {
          parent_category: {
            id: parent_category_id
          }
        }
      }
    })
    if (cate) {
      throw new AppError('BAD_REQUEST', 'INVALID_PARENT_CATEGORY')
    }
  }

  const file = req.file as Express.MulterS3.File
  try {
    const checkExistedCategory = await prisma.category.findFirst({
      where: {
        name,
        parent_category_id: parent_category_id
      }
    })

    if (checkExistedCategory) {
      throw new AppError('BAD_REQUEST', 'CATEGORY_EXISTED')
    }

    //check parent category exist?
    if (parent_category_id) {
      const checkParentCategory = await prisma.category.findFirst({
        where: {
          id: parent_category_id
        }
      })
      if (!checkParentCategory) {
        throw new AppError('BAD_REQUEST', 'PARENT_CATEGORY_NOT_FOUND')
      }
    }
    // check child category valid?
    if (child_categories_id) {
      const checkChildCategories = await prisma.category.findMany({
        where: {
          id: {
            in: child_categories_id.map((id) => parseInt(id))
          }
        }
      })
      if (checkChildCategories.length !== child_categories_id.length) {
        throw new AppError('BAD_REQUEST', 'CHILD_CATEGORY_NOT_FOUND')
      }
    }

    const category = await prisma.category.create({
      data: {
        name,
        parent_category_id: parent_category_id,
        image: file && file.location ? file.location : null
      }
    })

    await session.executeWrite((tx) =>
      tx.run(
        'MERGE (m:Category {id: $id, name: $name}) WITH m MATCH (n:Category {id: $parent_category_id}) MERGE (m)-[:HAS_PARENT]->(n) RETURN m, n',
        { id: category.id, name, parent_category_id }
      )
    )
    if (child_categories_id && child_categories_id.length > 0) {
      await session.executeWrite((tx) =>
        tx.run(
          'MATCH (m:Category) WHERE m.id IN $child_categories_id MATCH (n: Category {id: $id}) MERGE (m) -[:HAS_PARENT]-> (n)',
          { id: category.id, child_categories_id: child_categories_id.map((id) => parseInt(id)) }
        )
      )

      await prisma.category.updateMany({
        data: {
          parent_category_id: category.id
        },
        where: {
          id: {
            in: child_categories_id.map((id) => parseInt(id))
          }
        }
      })
    }
    return res.json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const updateCategory = async (req: Request, res: Response, next: NextFunction) => {
  const { category_id, child_categories_id, parent_category_id, name }: IUpdateCategory = req.body
  const file = req.file as Express.MulterS3.File

  try {
    if (parent_category_id && parseInt(parent_category_id) === parseInt(category_id)) {
      throw new AppError('BAD_REQUEST', 'INVALID_PARAMS')
    }
    if (parent_category_id) {
      const cate = await prisma.category.findFirst({
        where: {
          parent_category: {
            parent_category: {
              id: parseInt(parent_category_id)
            }
          }
        }
      })
      if (cate) {
        throw new AppError('BAD_REQUEST', 'INVALID_PARENT_CATEGORY')
      }
    }
    const category = await prisma.category.findFirst({
      where: {
        id: parseInt(category_id)
      }
    })
    if (parent_category_id && parseInt(parent_category_id) === parseInt(category_id)) {
      throw new AppError('BAD_REQUEST', 'INVALID_PARAMS')
    }
    if (!category) {
      throw new AppError('NOT_FOUND', 'NOT_FOUND')
    }

    if (category?.image && file) {
      await deleteImage(category.image)
    }

    await prisma.category.update({
      data: {
        parent_category_id: parent_category_id ? parseInt(parent_category_id) : category.parent_category_id,
        image: file ? file.location : category.image,
        name: name ? name : category.name
      },
      where: {
        id: parseInt(category_id)
      }
    })
    await session.run('MATCH (m:Category {id: $id}) SET m.name = $name', {
      id: parseInt(category_id),
      name: name ? name : category.name
    })

    // Nếu thay đổi parent_category_id thì cập nhật lại mối quan hệ
    if (parent_category_id && parseInt(parent_category_id) !== category.parent_category_id) {
      // Xoá quan hệ cũ
      await session.executeWrite((tx) =>
        tx.run('MATCH (m:Category {id: $id})-[r:HAS_PARENT]->() DELETE r', { id: parseInt(category_id) })
      )
      // Tạo quan hệ mới
      await session.executeWrite((tx) =>
        tx.run(
          'MATCH (m:Category {id: $id}) MATCH (n:Category {id: $parent_category_id}) MERGE (m)-[:HAS_PARENT]->(n)',
          { id: parseInt(category_id), parent_category_id: parseInt(parent_category_id) }
        )
      )
    }

    if (child_categories_id && child_categories_id.length > 0) {
      // Delete old parent_category_id
      await prisma.category.updateMany({
        data: {
          parent_category_id: null
        },
        where: {
          parent_category_id: parseInt(category_id)
        }
      })

      await session.executeWrite((tx) =>
        tx.run('MATCH ()-[r:HAS_PARENT]->(m:Category {id: $id}) DELETE r', { id: parseInt(category_id) })
      )

      // Add new old parent_category_id
      await prisma.category.updateMany({
        data: {
          parent_category_id: parseInt(category_id)
        },
        where: {
          id: {
            in: child_categories_id.map((id) => parseInt(id))
          }
        }
      })
      await session.executeWrite((tx) =>
        tx.run('MATCH (m:Category) WHERE m.id IN $child_categories_id MATCH (n:Category {id: $parent_category_id})', {
          child_categories_id: child_categories_id.map((id) => parseInt(id)),
          parent_category_id: parseInt(category_id)
        })
      )
    }
    return res.json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const deleteCategory = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { category_id } = req.query as IDeleteCategory

    // Nếu category có child category thì không thể xóa
    const childCategory = await prisma.category.findFirst({
      where: {
        parent_category_id: parseInt(category_id)
      }
    })
    if (childCategory) {
      throw new AppError('BAD_REQUEST', 'CATEGORY_HAS_CHILD')
    }
    const category = await prisma.category.delete({
      where: {
        id: parseInt(category_id)
      }
    })
    await session.executeWrite((tx) =>
      tx.run('MATCH (m:Category {id: $id}) DETACH DELETE m', { id: parseInt(category_id) })
    )
    if (category && category?.image) {
      await deleteImage(category.image)
    }
    return res.json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}
