import logger from '@/libs/logger'
import { resultTemplate } from '@/utils/result-template'
import { NextFunction, Request, Response } from 'express'
import { CODE, MessageKeyType } from './constants'
import { AppError } from './appError'

export const appErrorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof AppError) {
    res.status(CODE[err.statusCode]).json(resultTemplate(err.statusCode, err.message as MessageKeyType))
  } else {
    next(err)
  }
}

export const errorHandler = (err: any, req: Request, res: Response, _next: NextFunction) => {
  if (err.message === 'jwt expired') {
    return res.status(CODE.UNAUTHORIZED).json(resultTemplate('UNAUTHORIZED', 'UNAUTHORIZED'))
  }
  logger.error(err)
  return res
    .status(CODE.INTERNAL_SERVER_ERROR)
    .json(resultTemplate('INTERNAL_SERVER_ERROR', 'SERVER_ERROR', { description: err.message }))
}
