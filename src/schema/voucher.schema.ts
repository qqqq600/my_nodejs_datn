import { Static, Type } from '@sinclair/typebox'
import {
  addressSchema,
  emailSchema,
  nameSchema,
  numberSchema,
  passwordSchema,
  phoneNumberSchema
} from './common.schema'

export const createVoucherVariantSchema = Type.Object({
  name: nameSchema,
  started_at: Type.Number(),
  expired_at: Type.Number(),
  remaining_quantity: Type.Number({ minimum: 1 }),
  max_price: Type.Number({ minimum: 0 }),
  min_price_apply: Type.Number({ minimum: 0 }),
  percent_discount: Type.Number({ minimum: 0, maximum: 100 }),
  type: Type.String(),
  variants_id: Type.Array(Type.Number()),
  users_id: Type.Optional(Type.Array(Type.Number()))
})
export type ICreateVoucherVariant = Static<typeof createVoucherVariantSchema>
export const createVoucherProductSchema = Type.Object({
  name: nameSchema,
  started_at: Type.Number(),
  expired_at: Type.Number(),
  remaining_quantity: Type.Number({ minimum: 1 }),
  max_price: Type.Number({ minimum: 0 }),
  min_price_apply: Type.Number({ minimum: 0 }),
  percent_discount: Type.Number({ minimum: 0, maximum: 100 }),
  products_id: Type.Array(Type.Number()),
  type: Type.String(),
  users_id: Type.Optional(Type.Array(Type.Number()))
})
export type ICreateVoucherProduct = Static<typeof createVoucherProductSchema>
export const createVoucherCategorySchema = Type.Object({
  name: nameSchema,
  started_at: Type.Number(),
  expired_at: Type.Number(),
  remaining_quantity: Type.Number({ minimum: 1 }),
  max_price: Type.Number({ minimum: 0 }),
  min_price_apply: Type.Number({ minimum: 0 }),
  percent_discount: Type.Number({ minimum: 0, maximum: 100 }),
  categories_id: Type.Array(Type.Number()),
  type: Type.String(),
  users_id: Type.Optional(Type.Array(Type.Number()))
})
export type ICreateVoucherCategory = Static<typeof createVoucherCategorySchema>

export const createVoucherAllSchema = Type.Object({
  name: nameSchema,
  started_at: Type.Number(),
  expired_at: Type.Number(),
  remaining_quantity: Type.Number({ minimum: 1 }),
  max_price: Type.Number({ minimum: 0 }),
  min_price_apply: Type.Number({ minimum: 0 }),
  percent_discount: Type.Number({ minimum: 0, maximum: 100 }),
  type: Type.String(),
  users_id: Type.Optional(Type.Array(Type.Number()))
})
export type ICreateVoucherAll = Static<typeof createVoucherAllSchema>

export const createVoucherSchema = Type.Union([
  createVoucherVariantSchema,
  createVoucherProductSchema,
  createVoucherCategorySchema,
  createVoucherAllSchema
])

export type ICreateVoucher = Static<typeof createVoucherSchema>

export const deleteVoucherSchema = Type.Object({
  voucher_id: numberSchema
})
export type IDeleteVoucher = Static<typeof deleteVoucherSchema>

export const updateVoucherInfoSchema = Type.Object({
  voucher_id: Type.Number(),
  name: nameSchema,
  started_at: Type.Number(),
  expired_at: Type.Number(),
  remaining_quantity: Type.Number({ minimum: 1 }),
  max_price: Type.Number({ minimum: 0 }),
  min_price_apply: Type.Number({ minimum: 0 }),
  percent_discount: Type.Number({ minimum: 0, maximum: 100 })
})
export type IUpdateVoucherInfo = Static<typeof updateVoucherInfoSchema>

export const updateVoucherVariantSchema = Type.Object({
  variants_id: Type.Array(Type.Number()),
  voucher_id: Type.Number(),
  type: Type.String()
})
export type IUpdateVoucherVariant = Static<typeof updateVoucherVariantSchema>

export const updateVoucherProductSchema = Type.Object({
  products_id: Type.Array(Type.Number()),
  voucher_id: Type.Number(),
  type: Type.String()
})
export type IUpdateVoucherProduct = Static<typeof updateVoucherProductSchema>

export const updateVoucherCategorySchema = Type.Object({
  categories_id: Type.Array(Type.Number()),
  voucher_id: Type.Number(),
  type: Type.String()
})
export type IUpdateVoucherCategory = Static<typeof updateVoucherCategorySchema>

export const updateVoucherAllSchema = Type.Object({
  voucher_id: Type.Number(),
  type: Type.String()
})
export type IUpdateVoucherAll = Static<typeof updateVoucherAllSchema>

export const updateVoucherRangeSchema = Type.Union([
  updateVoucherVariantSchema,
  updateVoucherProductSchema,
  updateVoucherCategorySchema,
  updateVoucherAllSchema
])

export type IUpdateVoucherRange = Static<typeof updateVoucherRangeSchema>

export const updateUserOwnVoucherSchema = Type.Object({
  voucher_id: Type.Number(),
  users_id: Type.Optional(Type.Array(Type.Number()))
})
export type IUpdateUserOwnVoucher = Static<typeof updateUserOwnVoucherSchema>

export const getVoucherDetailSchema = Type.Object({
  voucher_id: numberSchema
})
export type IGetVoucherDetail = Static<typeof getVoucherDetailSchema>
