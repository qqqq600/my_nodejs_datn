`
type User {
id: ID @unique
phone_number: String @unique
email: String
product_bought: [Product!]! @relationship(type: "BOUGHT", properties: "ProductBought", direction: OUT)
wish_list: [Product!]! @relationship(type: "ADD_TO_WISH_LIST", properties: "ProductWishList", direction: OUT)
product_viewed: [Product!]! @relationship(type: "VIEWED", properties: "ProductViewd", direction: OUT)
product_cart: [Product!]! @relationship(type: "ADD_TO_CART", direction: OUT)
}

    type Product {
        id: ID @unique
        name: String!
        pattern: String!
        style: String!
        category: [Category!]! @relationship(type: "IS_IN", direction: OUT)
    }

    type Category{
      id: ID @unique
      name: String!
      parent_category: Category @relationship(type: "HAS_PARENT", direction: OUT)
    }

    interface ProductBought @relationshipProperties {
        bought_at: Date!
        quantity: Int!
    }

    interface ProductViewd @relationshipProperties {
        viewed_at: Date!
        count: Int!
    }

    interface ProductWishList @relationshipProperties {
        add_at: Date!

    }
    interface ProductInCart @relationshipProperties {
        add_at: Date!
        variant_id: Int!
        color_id: Int!
        quantity: Int!
    }

`

`Category {id, name} -[:HAS_PARENT]-> Category {id, name}
Product {id, name} -[:IS_IN]-> Category {id, name}
Product {id, name} -[:HAS]-> Variant {id, quantity}
User {id, email} -[:IN_CART {quantity, created_at}]-> Variant {id}
User {id, email} -[:IN_WISH {created_at}]-> Product {id}

User -[:BOUGHT: {created_at}]-> Variant`
