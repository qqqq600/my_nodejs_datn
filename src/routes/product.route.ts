import { uploadImage } from './../services/s3.service'
import express, { Express } from 'express'
import validate from '@/middlewares/validate.middleware'
import {
  createProductSchema,
  createVariantSchema,
  getProductSchema,
  updateProductSchema,
  getProductDetailSchema,
  deleteProductSchema,
  getVariantSchema,
  updateVariantSchema,
  deleteVariantImageSchema,
  createVariantImageSchema
} from '@/schema/product.schema'
import {
  createProduct,
  createVariant,
  createVariantImage,
  deleteProduct,
  deleteVariant,
  deleteVariantImage,
  getColor,
  getProductDetailData,
  getProducts,
  getVariant,
  updateProduct,
  updateVariant
} from '@/controllers/product.controller'
const router = express.Router()

const ProductRoute = (app: Express) => {
  router.post('/', validate({ schema: createProductSchema, part: 'body' }), createProduct)
  router.patch('/', validate({ schema: updateProductSchema, part: 'body' }), updateProduct)
  router.delete('/', validate({ schema: deleteProductSchema, part: 'query' }), deleteProduct)

  router.get('/variant', validate({ schema: getVariantSchema, part: 'query' }), getVariant)
  router.post(
    '/variant',
    uploadImage('product', 3).array('image'),
    validate({ schema: createVariantSchema, part: 'body' }),
    createVariant
  )
  router.patch('/variant', validate({ schema: updateVariantSchema, part: 'body' }), updateVariant)
  router.delete('/variant', validate({ schema: deleteProductSchema, part: 'query' }), deleteVariant)
  router.patch(
    '/variant/image',
    uploadImage('product', 3).array('image'),
    validate({ schema: createVariantImageSchema, part: 'body' }),
    createVariantImage
  )
  router.delete('/variant/image', validate({ schema: deleteVariantImageSchema, part: 'query' }), deleteVariantImage)

  router.post('/filter', validate({ schema: getProductSchema, part: 'body' }), getProducts)
  router.get('/detail', validate({ schema: getProductDetailSchema, part: 'query' }), getProductDetailData)
  router.get('/color', getColor)
  return app.use('/product', router)
}

export default ProductRoute
