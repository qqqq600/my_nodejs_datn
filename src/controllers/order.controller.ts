import { NextFunction, Request, Response } from 'express'
import { AppError } from '@/config/appError'
import { resultTemplate } from '@/utils/result-template'
import prisma from '@/libs/prisma'
import dayjs from 'dayjs'
import {
  ICreateOrder,
  IGetOrderAdmin,
  IGetOrderDetail,
  IGetOrderOfUser,
  IOrderDetailOfUserNotYetReview,
  IUpdateOrderState,
  IUpdatePaymentMethod
} from '@/schema/order.schema'
import { ORDER_STATE } from '@/config/constants'
import {
  getOrderWithStateServiceForUser,
  createOrderService,
  cancelOrderService,
  getOrderWithStateServiceForAdmin
} from '@/services/order.service'
import { session } from '@/neo4j/configDbNeo4j'
import { payos } from '@/libs/payos'

export const createOrder = async (req: Request, res: Response, next: NextFunction) => {
  const orderInfomation = req.body as ICreateOrder
  const user = req.user
  const now = dayjs()
  try {
    const checkRecipient = await prisma.recipient_information.findFirst({
      where: {
        user_id: user?.id,
        id: orderInfomation.recipient_information_id
      }
    })
    if (!checkRecipient) {
      throw new AppError('BAD_REQUEST', 'RECIPIENT_NOT_FOUND')
    }

    orderInfomation.variant_order.forEach(async (item) => {
      let checkVariant = await prisma.variant.findFirst({
        where: {
          remain_quantity: {
            gte: item.quantity
          },
          id: item.variant_id
        }
      })
      if (!checkVariant) {
        throw new AppError('BAD_REQUEST', 'VARIANT_NOT_ENOUGH')
      }
    })
    if (orderInfomation.voucher_id) {
      const checkVoucher = await prisma.voucher.findFirst({
        where: {
          started_at: {
            lte: now.toDate()
          },
          expired_at: {
            gte: now.toDate()
          },
          user_own_voucher: {
            some: {
              user_id: user?.id,
              status: false
            }
          },
          id: orderInfomation.voucher_id,
          remaining_quantity: {
            gt: 0
          }
        }
      })
      // Kiểm tra xem voucher đó có hợp lệ không?
      if (!checkVoucher) {
        throw new AppError('BAD_REQUEST', 'INVALID_VOUCHER')
      }
      // update quantity voucher (-1)
      await prisma.voucher.update({
        where: {
          id: orderInfomation.voucher_id
        },
        data: {
          remaining_quantity: {
            decrement: 1
          },
          used_quantity: {
            increment: 1
          }
        }
      })
    }

    // create Order
    const newOrder = await prisma.order.create({
      data: {
        state: ORDER_STATE.WAITING,
        description: orderInfomation.description || null,
        amount: orderInfomation.amount,
        voucher_id: orderInfomation.voucher_id || null,
        recipient_information_id: orderInfomation.recipient_information_id,
        shipping_method_id: orderInfomation.shipping_method_id,
        payment_method_id: orderInfomation.payment_method_id,
        created_at: new Date()
      }
    })

    const paymentLink = await payos.createPaymentLink({
      cancelUrl: 'https://e-commerce-project-42o.pages.dev/checkout',
      returnUrl: 'https://e-commerce-project-42o.pages.dev/checkout',
      orderCode: newOrder.id,
      amount: orderInfomation.amount,
      description: `Thanh toán đơn hàng`
    })
    const updateOrder = await prisma.order.update({
      where: {
        id: newOrder.id
      },
      data: {
        payment_link: paymentLink
      }
    })
    //create orderDetail and update variant quantity
    for (let i = 0; i < orderInfomation.variant_order.length; i++) {
      let item = orderInfomation.variant_order[i]
      await prisma.order_detail.create({
        data: {
          quantity: item.quantity,
          order_id: newOrder.id,
          price: item.price,
          variant_id: item.variant_id
        }
      })

      const variant = await prisma.variant.update({
        where: {
          id: item.variant_id
        },
        data: {
          remain_quantity: {
            decrement: item.quantity
          }
        }
      })

      await session.run(
        'MATCH (v:Variant {id: $variant_id}) SET v.quantity = $quantity WITH v MATCH (u:User {id: $user_id}) MERGE (u) -[:BOUGHT {created_at: $created_at}]-> (v)',
        {
          variant_id: variant.id,
          quantity: variant.remain_quantity,
          user_id: user?.id,
          created_at: dayjs().unix()
        }
      )
      await session.run('MATCH (u:User {id: $user_id})-[r:IN_CART]->(v:Variant {id: $variant_id}) DELETE r', {
        user_id: user?.id,
        variant_id: item.variant_id
      })
    }

    createOrderService(orderInfomation.variant_order)

    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', updateOrder))
  } catch (error: any) {
    console.log(error)
    next(error)
  }
}

export const getOrderOfUser = async (req: Request, res: Response, next: NextFunction) => {
  const user = req.user
  try {
    if (!user) {
      throw new AppError('BAD_REQUEST', 'ACCOUNT_NOT_FOUND')
    }
    const { state, page, limit } = req.query as IGetOrderOfUser
    const orders = await getOrderWithStateServiceForUser({
      state,
      user_id: user.id,
      page: page ? parseInt(page) : 1,
      limit: limit ? parseInt(limit) : 5
    })
    return res.json(resultTemplate('SUCCESS', 'SUCCESS', orders))
  } catch (error: any) {
    next(error)
  }
}

export const getOrderAdmin = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { state, page, limit, user_id } = req.query as IGetOrderAdmin
    const orders = await getOrderWithStateServiceForAdmin({
      state: state,
      page: page ? parseInt(page) : undefined,
      limit: limit ? parseInt(limit) : undefined,
      user_id: user_id ? parseInt(user_id) : undefined
    })
    return res.json(resultTemplate('SUCCESS', 'SUCCESS', orders))
  } catch (error: any) {
    next(error)
  }
}

export const getOrderDetail = async (req: Request, res: Response, next: NextFunction) => {
  const { order_id } = req.query as IGetOrderDetail
  try {
    let result = await prisma.order.findFirst({
      where: {
        id: parseInt(order_id)
      },
      include: {
        recipient_information: {
          select: {
            address: true,
            id: true,
            phone_number: true,
            name: true
          }
        },
        payment_method: true,
        shipping_method: true,
        voucher: {
          select: {
            id: true,
            name: true
          }
        },
        order_detail: {
          select: {
            id: true,
            price: true,
            quantity: true,
            variant: {
              select: {
                color: {
                  select: {
                    id: true,
                    name: true
                  }
                },
                product: {
                  select: {
                    id: true,
                    name: true,
                    product_image: true
                  }
                },
                id: true,
                size: true
              }
            }
          }
        }
      }
    })

    if (!result) {
      throw new AppError('BAD_REQUEST', 'ORDER_NOT_FOUND')
    }
    result.order_detail.map((detail, i) => {
      if (result) {
        result.order_detail[i].variant.product.product_image = [
          detail.variant.product.product_image.filter((image) => image.color_id === detail.variant.color.id)[0]
        ]
      }
    })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', result))
  } catch (error: any) {
    next(error)
  }
}

export const updateOrderStateAdmin = async (req: Request, res: Response, next: NextFunction) => {
  const { order_id, state } = req.query as IUpdateOrderState
  try {
    const checkOrder = await prisma.order.findFirst({
      where: {
        id: parseInt(order_id)
      }
    })
    if (!checkOrder) {
      throw new AppError('BAD_REQUEST', 'ORDER_NOT_FOUND')
    }
    if (checkOrder.state === ORDER_STATE.CANCEL) {
      throw new AppError('BAD_REQUEST', 'ORDER_WAS_CANCELLED')
    }
    if (checkOrder.state === ORDER_STATE.DONE) {
      throw new AppError('BAD_REQUEST', 'ORDER_IS_COMPLETED')
    }
    if (checkOrder.state === ORDER_STATE.WAITING && (state === ORDER_STATE.CONFIRM || state === ORDER_STATE.CANCEL)) {
      if (state === ORDER_STATE.CANCEL) {
        const year = dayjs(checkOrder.created_at).year()
        const month = dayjs(checkOrder.created_at).month() + 1
        cancelOrderService(checkOrder.id, month, year)
        await prisma.order.update({
          where: {
            id: checkOrder.id
          },
          data: {
            state: state,
            cancel_at: new Date()
          }
        })
      } else {
        await prisma.order.update({
          where: {
            id: checkOrder.id
          },
          data: {
            state: state,
            confirm_at: new Date()
          }
        })
      }
    } else if (checkOrder.state === ORDER_STATE.CONFIRM && state === ORDER_STATE.SHIPPING) {
      await prisma.order.update({
        where: {
          id: checkOrder.id
        },
        data: {
          state: state,
          ship_at: new Date()
        }
      })
    } else if (checkOrder.state === ORDER_STATE.SHIPPING && state === ORDER_STATE.DONE) {
      if (checkOrder.payment_method_id == 1) {
        await prisma.order.update({
          where: {
            id: checkOrder.id
          },
          data: {
            state: state,
            done_at: new Date(),
            transaction_at: new Date()
          }
        })
      } else {
        await prisma.order.update({
          where: {
            id: checkOrder.id
          },
          data: {
            state: state,
            done_at: new Date()
          }
        })
      }
    } else {
      throw new AppError('BAD_REQUEST', 'INVALID_PARAMS')
    }
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const userCancelOrder = async (req: Request, res: Response, next: NextFunction) => {
  const { order_id } = req.query as IUpdateOrderState
  const user = req.user
  try {
    const order = await prisma.order.findFirst({
      where: {
        id: parseInt(order_id),
        recipient_information: {
          user_id: user?.id
        }
      }
    })
    if (!order) {
      throw new AppError('BAD_REQUEST', 'ORDER_NOT_FOUND')
    }
    if (order.state === ORDER_STATE.CANCEL) {
      throw new AppError('BAD_REQUEST', 'ORDER_WAS_CANCELLED')
    }
    if (order.state === ORDER_STATE.DONE) {
      throw new AppError('BAD_REQUEST', 'ORDER_IS_COMPLETED')
    }
    if (order.state !== ORDER_STATE.WAITING) {
      throw new AppError('BAD_REQUEST', 'ORDER_CAN_NOT_CANCEL')
    }
    const record = await prisma.order.update({
      where: {
        id: order.id
      },
      data: {
        state: ORDER_STATE.CANCEL,
        cancel_at: new Date()
      }
    })
    const year = dayjs(record.created_at).year()
    const month = dayjs(record.created_at).month() + 1
    cancelOrderService(order.id, month, year)

    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const payosTransferHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    console.log('payment handler')
    const webhookData = payos.verifyPaymentWebhookData(req.body)

    if (['Ma giao dich thu nghiem', 'VQRIO123'].includes(webhookData.description)) {
      return res.json(resultTemplate('SUCCESS', 'SUCCESS'))
    }
    const io = req.io
    if (io) {
      io.to(`order_${webhookData.orderCode}`).emit('paymentUpdated', {
        orderId: webhookData.orderCode,
        status: 'PAID'
      })
    }
    await prisma.order.update({
      where: {
        id: webhookData.orderCode
      },
      data: {
        transaction_at: dayjs(webhookData.transactionDateTime).toDate(),
        webhook_data: webhookData
      }
    })

    return res.json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error) {
    next(error)
  }
}

export const orderDetailOfUserNotYetReview = async (req: Request, res: Response, next: NextFunction) => {
  const user = req.user
  const { limit, page } = req.query as IOrderDetailOfUserNotYetReview
  try {
    if (!user) {
      throw new AppError('BAD_REQUEST', 'USER_NOT_FOUND')
    }
    const result = await prisma.order_detail.findMany({
      where: {
        order: {
          recipient_information: {
            user_id: user.id
          },
          state: ORDER_STATE.DONE
        },
        review: {
          none: {}
        }
      },
      skip: page ? (parseInt(page) - 1) * parseInt(limit ? limit : '10') : undefined,
      take: limit ? parseInt(limit) : 10,
      orderBy: {
        order: {
          done_at: 'desc'
        }
      },
      include: {
        variant: {
          select: {
            color: {
              select: {
                id: true,
                name: true
              }
            },
            product: {
              select: {
                id: true,
                product_image: true,
                name: true
              }
            },
            size: true
          }
        }
      }
    })

    result.map((detail, i) => {
      result[i].variant.product.product_image = [
        detail.variant.product.product_image.filter((image) => image.color_id === detail.variant.color.id)[0]
      ]
    })

    return res.json(resultTemplate('SUCCESS', 'SUCCESS', result))
  } catch (error: any) {
    next(error)
  }
}

export const updatePaymentMethod = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { order_id, payment_method_id } = req.body as IUpdatePaymentMethod
    const { user } = req
    const order = await prisma.order.findFirst({
      where: {
        id: order_id,
        recipient_information: {
          user_id: user?.id
        }
      }
    })
    if (!order) {
      throw new AppError('BAD_REQUEST', 'ORDER_NOT_FOUND')
    }
    if (order.transaction_at) {
      throw new AppError('BAD_REQUEST', 'CANT_CHANGE_PAYMENT_METHOD')
    }
    const result = await prisma.order.update({
      where: {
        id: order_id
      },
      data: {
        payment_method_id: payment_method_id
      }
    })
    return res.json(resultTemplate('SUCCESS', 'SUCCESS', result))
  } catch (error) {
    next(error)
  }
}
