import dayjs from 'dayjs'
import nodemailer from 'nodemailer'

export const resetPwdUserOtpService = (email: string, otp_code: string, name: string | null) => {
  const html: string = `<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
  <div style="margin:50px auto;width:70%;padding:20px 0">
    <div style="border-bottom:1px solid #eee">
      <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Fashion Fushion</a>
    </div>
    <p style="font-size:1.1em">Hi ${name}</p>
    <p>Thank you for choosing Fashion Fushion. Use the following OTP to complete your reset password procedures. OTP is valid for 20 minutes. Do not share this code with others.</p>
    <h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">${otp_code}</h2>
    <p>If you did not request this, you can ignore this email.</p>
    <p style="font-size:0.9em;">Regards,<br />Fashion Fushion</p>
    <hr style="border:none;border-top:1px solid #eee" />
    <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
      <p>Fashion Fushion Inc</p>
      <p>BachKhoa TPHCM</p>
      <p>HCMVN</p>
    </div>
  </div>
</div>`
  sendMail(email, html, 'OTP Verification')
}

export const sendMail = async (email: string, html: string, subject: string) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.USER_MAIL,
      pass: process.env.GEN_PASS
    }
  })
  const mailOptions = {
    from: 'FashionFushion suport<suport@gmail.com>',
    to: email,
    subject: subject,
    html: html
  }
  await transporter.sendMail(mailOptions)
}

export const resetPwdUserSuccessService = (email: string, name: string | null, passwordChangeDate: Date) => {
  const html: string = `<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
  <div style="margin:50px auto;width:70%;padding:20px 0">
    <div style="border-bottom:1px solid #eee">
      <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Fashion Fushion</a>
    </div>
    <p style="font-size:1.1em">Hi ${name}</p>
    <p>Thank you for choosing Fashion Fushion. We are writing to inform you that your password has been successfully reset.</p>
    <p>Password Change Date: <strong>${dayjs(passwordChangeDate).format('DD/MM/YYYY HH:mm')}</strong></p>
    <p>If you did not perform this action, please contact our support team immediately.</p>
    <p style="font-size:0.9em;">Regards,<br />Fashion Fushion</p>
    <hr style="border:none;border-top:1px solid #eee" />
    <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
      <p>Fashion Fushion Inc</p>
      <p>BachKhoa TPHCM</p>
      <p>HCMVN</p>
    </div>
  </div>
</div>`
  sendMail(email, html, 'Password Reset Success')
}
