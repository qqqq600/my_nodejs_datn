import { Request, Response, NextFunction } from 'express'
import Ajv from 'ajv'
import addFormats from 'ajv-formats'
import logger from '../libs/logger'
import { CODE } from '@/config/constants'

const ajv = addFormats(new Ajv({}), [
  'date-time',
  'time',
  'date',
  'email',
  'hostname',
  'ipv4',
  'ipv6',
  'uri',
  'uri-reference',
  'uuid',
  'uri-template',
  'regex',
  'password',
  'url',
  'int32',
  'int64'
])

type RequestPart = 'body' | 'query' | 'params'

interface ValidateOptions {
  schema: any
  part: RequestPart
}

export default function validate({ schema, part }: ValidateOptions) {
  const validate = ajv.compile(schema)
  return (req: Request, res: Response, next: NextFunction) => {
    let data = req[part]
    const valid = validate(data)
    if (!valid) {
      const result = {
        code: CODE.BAD_REQUEST,
        message: validate.errors
      }
      logger.error(data)
      return res.status(400).json(result)
    } else next()
  }
}
