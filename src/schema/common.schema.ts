import { Type } from '@sinclair/typebox'

export const nameSchema = Type.String({ minLength: 1, maxLength: 100 })

export const emailSchema = Type.String({ format: 'email' })

export const phoneNumberSchema = Type.String({ pattern: '^[0-9]{10,11}$' })

export const passwordSchema = Type.String({ minLength: 6, maxLength: 20 })

export const addressSchema = Type.String({ minLength: 1, maxLength: 100 })

export const numberSchema = Type.String({ pattern: '^[1-9][0-9]*$' })
