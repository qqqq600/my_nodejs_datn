import { sign, verify } from 'jsonwebtoken'

const JWT_KEY = process.env.JWT_KEY || 'secret'

export function createJSONToken(id: number) {
  return sign({ id }, JWT_KEY, { expiresIn: '7d' })
}

export function validateJSONToken(token: string) {
  return verify(token, JWT_KEY)
}

export function parseJwt(token: string) {
  return JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString())
}
