import prisma from '@/libs/prisma'

export default class FakeReviewService {
  static importReviews = async (
    productId: number,
    images: string[],
    reviews: {
      name: string
      content: string
    }[]
  ) => {
    const stars = [3, 4, 5]

    return await prisma.fake_review_product.createMany({
      data: reviews.map((review, idx) => ({
        username: review?.name,
        content: review?.content,
        product_id: productId,
        avatar: images[idx],
        star: stars?.[Math.floor(Math.random() * 3)] ?? 5
      }))
    })
  }

  static getAllProducts = async () => {
    return await prisma.product.findMany({
      include: {
        variants: true,
        product_image: true
      }
    })
  }
  static getProductById = async (productId: number) => {
    return await prisma.product.findFirst({
      where: {
        id: productId
      },
      include: {
        variants: true,
        product_image: true,
        Fake_review_product: true
      }
    })
  }

  static getReviewsByProductId = async (productId: number) => {
    return await prisma.fake_review_product.findMany({
      where: {
        product_id: productId
      }
    })
  }
}
