import FakeReviewService from '@/services/fake-review.service'
import { NextFunction, Request, Response } from 'express'

interface IFakeReview {
  name: string
  content: string
}
export default class FakeReviewController {
  static importReviews = async (req: Request, res: Response) => {
    const data = req?.body as {
      productId: number
      reviews: IFakeReview[]
      images: string[]
    }
    return res.status(200).json(FakeReviewService.importReviews(data?.productId, data?.images, data?.reviews))
  }
  static getAllProducts = async (req: Request, res: Response) => {
    return res.status(200).json(await FakeReviewService.getAllProducts())
  }
  static getReviewsByProductId = async (req: Request, res: Response) => {
    return res.status(200).json(await FakeReviewService.getProductById(Number(req.params?.id)))
  }
}
