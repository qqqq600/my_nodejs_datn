export const CODE = {
  SUCCESS: 200,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  NOT_FOUND: 404,
  INTERNAL_SERVER_ERROR: 500,
  SERVICE_UNAVAILABLE: 503,
  GATEWAY_TIMEOUT: 504
} as const

export type CodeKeyType = keyof typeof CODE

export const MESSAGE = {
  SUCCESS: 'Success',
  FAILED: 'Failed',
  SERVER_ERROR: 'Internal Server Error',
  NOT_FOUND: 'Not Found',
  HAVE_AN_ERROR: 'Have an error',
  UNAUTHORIZED: 'Unauthorized',
  EMAIL_EXISTED: 'Email existed',
  INCORRECT_PASSWORD: 'Incorrect password',
  INCORRECT_EMAIL: 'Incorrect email',
  INCORRECT_TOKEN: 'Incorrect token',
  EXPIRED_TOKEN: 'Expired token',
  ACCOUNT_NOT_FOUND: 'Account not found',
  ACCOUNT_NOT_ACTIVE: 'Account not active',
  FILE_TYPE_NOT_ALLOWED: 'File type not allowed',
  INVALID_PARAMS: 'Invalid params',
  CATEGORY_EXISTED: 'Category existed',
  INVALID_VARIANT_SIZE: 'Invalid variant size',
  VARIANT_EXISTED: 'Variant existed',
  CATEGORY_HAS_CHILD: 'Category has child',
  INVALID_STAFF_ROLE: 'Invalid staff role',
  PHONE_NUMBER_EXISTED: 'Phone number existed',
  NOT_ADMIN_ACCOUNT: 'Not admin account',
  THIS_IS_NOT_YOURS: 'This is not yours',
  VOUCHER_NOT_FOUND: 'Voucher not found',
  INVALID_PRICE: 'Sale Price must be less than Export Price',
  PRODUCT_NOT_EXISTED: 'Product not existed',
  VARIANT_NOT_EXISTED: 'Variant not existed',
  PRODUCT_ALREADY_EXISTED: 'Product already existed',
  RECIPIENT_NOT_FOUND: 'Recipient not found',
  INVALID_VOUCHER: 'Invalid voucher',
  ORDER_NOT_FOUND: 'Order not found',
  ORDER_WAS_CANCELLED: 'Order was cancelled',
  ORDER_IS_COMPLETED: 'Order was completed',
  VARIANT_NOT_ENOUGH: 'Variant not enough',
  USER_NOT_FOUND: 'User not found',
  REVIEW_EXISTED: 'Review already exists',
  ORDER_NOT_COMPLETED_YET: 'Order is not completed yet',
  RANGE_LIST_AND_TYPE_UNLIKE: 'Range list and type are unlike',
  ERROR_APPLICATION_TIME: 'Error application time',
  ERROR_PRICE: 'Error price',
  INVALID_VOUCHER_TYPE: 'Invalid voucher type',
  ORDER_ALREADY_PAID: 'Order already paid',
  STAFF_NOT_FOUND: 'Staff not found',
  REVIEW_NOT_FOUND: 'Review not found',
  INVALID_OTP_CODE: 'Invalid otp code',
  OTP_CODE_EXPIRED: 'Otp code expired',
  GOOGLE_TOKEN_REQUIRED: 'Google token required',
  GOOGLE_TOKEN_INVALID: 'Google token invalid',
  PRODUCT_HAS_ORDER: 'Product has order',
  ORDER_CAN_NOT_CANCEL: 'Order can not cancel',
  USER_HAS_ORDER: 'User has order',
  VARIANT_HAS_ORDER: 'Variant has order',
  INVALID_CATEGORY_LIST: 'Invalid category list',
  INVALID_PRODUCT_LIST: 'Invalid product list',
  INVALID_VARIANT_LIST: 'Invalid variant list',
  PARENT_CATEGORY_NOT_FOUND: 'Parent category not found',
  CHILD_CATEGORY_NOT_FOUND: 'Child category not found',
  INVALID_PARENT_CATEGORY: 'Invalid parent category',
  CANT_CHANGE_PAYMENT_METHOD: 'Can not change payment method'
} as const

export type MessageKeyType = keyof typeof MESSAGE

export const ORDER_STATE = {
  WAITING: 'WAITING',
  CONFIRM: 'CONFIRM',
  SHIPPING: 'SHIPPING',
  DONE: 'DONE',
  CANCEL: 'CANCEL'
}

export const VOUCHER_TYPE = {
  CATEGORY: 'CATEGORY',
  PRODUCT: 'PRODUCT',
  VARIANT: 'VARIANT',
  ALL: 'ALL'
}

export const VARIANT_SIZE = [
  'XS',
  'S',
  'M',
  'L',
  'XL',
  '2XL',
  '3XL',
  '35',
  '36',
  '37',
  '38',
  '39',
  '40',
  '41',
  '42',
  '43',
  '44',
  '45'
]

export const STAFF_ROLE = ['staff', 'admin']

export const PRODUCT_PATTERN = ['Họa tiết', 'Kẻ', 'Caro', 'Chấm bi', 'Sọc', 'Họa tiết khác']

export const PRODUCT_MATERIAL = ['Len', 'Thun', 'Vải', 'Da', 'Vải khác']

export const DASHBOARD_MODE = {
  THIS_MONTH: 'THIS_MONTH',
  THIS_WEEK: 'THIS_WEEK',
  LAST_WEEK: 'LAST_WEEK',
  LAST_MONTH: 'LAST_MONTH',
  ADVANCED: 'ADVANCED'
}

export const SORT = [
  'Đánh giá cao',
  'Giá tăng dần',
  'Giá giảm dần',
  'Mới nhất',
  'Phổ biến nhất',
  'Giảm giá nhiều nhất'
] as const
