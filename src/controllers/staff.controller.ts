import { AppError } from '@/config/appError'
import { DASHBOARD_MODE, ORDER_STATE, STAFF_ROLE } from '@/config/constants'
import prisma from '@/libs/prisma'
import { IDeleteUser, IGetBestSelling, IGetRevenue, IGetStaffDetail } from '@/schema/staff.schema'
import { ICreateStaff, IStaffLogin } from '@/schema/staff.schema'
import { createJSONToken } from '@/utils/jwt'
import { resultTemplate } from '@/utils/result-template'
import { where } from '@tensorflow/tfjs'
import { compare, hash } from 'bcryptjs'
import { NextFunction, Request, Response } from 'express'
import { ValueOrderOveview, countOrderOveview } from '../services/staff.service'
import dayjs from 'dayjs'

export const staffLogin = async (req: Request, res: Response, next: NextFunction) => {
  const staffLoginInfo: IStaffLogin = req.body
  try {
    const staff = await prisma.staff.findFirst({
      where: {
        email: staffLoginInfo.email
      }
    })
    if (!staff) {
      throw new AppError('NOT_FOUND', 'ACCOUNT_NOT_FOUND')
    }
    const isPassword = await compare(staffLoginInfo.password, staff.password)
    if (!isPassword) {
      throw new AppError('BAD_REQUEST', 'INCORRECT_PASSWORD')
    }
    const token = createJSONToken(staff.id)
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', { token: token, staff }))
  } catch (error: any) {
    next(error)
  }
}

export const createStaff = async (req: Request, res: Response, next: NextFunction) => {
  const { name, email, password, phone_number, role }: ICreateStaff = req.body
  try {
    if (role && STAFF_ROLE.includes(role) === false) {
      throw new AppError('BAD_REQUEST', 'INVALID_STAFF_ROLE')
    }
    const existingEmail = await prisma.staff.findFirst({
      where: {
        email: email
      }
    })
    if (existingEmail) {
      throw new AppError('BAD_REQUEST', 'EMAIL_EXISTED')
    }
    const existingPhoneNumber = await prisma.staff.findFirst({
      where: {
        phone_number: phone_number
      }
    })
    if (existingPhoneNumber) {
      throw new AppError('BAD_REQUEST', 'PHONE_NUMBER_EXISTED')
    }
    const hashedPw = await hash(password, 12)

    await prisma.staff.create({
      data: {
        name: name,
        email: email,
        password: hashedPw,
        phone_number: phone_number,
        role: role
      }
    })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const deleteUser = async (req: Request, res: Response, next: NextFunction) => {
  const { user_id } = req.query as IDeleteUser
  try {
    const user = await prisma.user.findFirst({
      where: {
        id: parseInt(user_id)
      }
    })
    if (!user) {
      throw new AppError('BAD_REQUEST', 'USER_NOT_FOUND')
    }

    const orders = await prisma.order.findFirst({
      where: {
        recipient_information: {
          user_id: parseInt(user_id)
        }
      }
    })
    if (orders) {
      throw new AppError('BAD_REQUEST', 'USER_HAS_ORDER')
    }
    await prisma.user_own_voucher.deleteMany({
      where: {
        user_id: parseInt(user_id)
      }
    })
    await prisma.recipient_information.deleteMany({
      where: {
        user_id: parseInt(user_id)
      }
    })
    await prisma.user.delete({
      where: {
        id: parseInt(user_id)
      }
    })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS'))
  } catch (error: any) {
    next(error)
  }
}

export const getAllStaff = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const staffs = await prisma.staff.findMany()
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', staffs))
  } catch (error: any) {
    next(error)
  }
}

export const getStaffDetail = async (req: Request, res: Response, next: NextFunction) => {
  const { staff_id } = req.query as IGetStaffDetail
  try {
    const staff = await prisma.staff.findFirst({
      where: {
        id: parseInt(staff_id)
      }
    })
    if (!staff) {
      throw new AppError('BAD_REQUEST', 'STAFF_NOT_FOUND')
    }
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', staff))
  } catch (error: any) {
    next(error)
  }
}

export const getOverView = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const [count, value] = await Promise.all([countOrderOveview(), ValueOrderOveview()])
    return res.status(200).json(
      resultTemplate('SUCCESS', 'SUCCESS', {
        count,
        value
      })
    )
  } catch (error: any) {
    next(error)
  }
}

export const getRevenue = async (req: Request, res: Response, next: NextFunction) => {
  const { mode, startDateInput, endDateInput } = req.query as IGetRevenue
  try {
    let startDate
    let endDate

    if (mode === 'THIS_MONTH') {
      startDate = dayjs().startOf('month').toDate()
      endDate = dayjs().endOf('month').toDate()
    } else if (mode === 'THIS_WEEK') {
      startDate = dayjs().startOf('week').toDate()
      endDate = dayjs().endOf('week').toDate()
    } else if (mode === 'LAST_WEEK') {
      startDate = dayjs().subtract(1, 'week').startOf('week').toDate()
      endDate = dayjs().subtract(1, 'week').endOf('week').toDate()
    } else if (mode == 'LAST_MONTH') {
      startDate = dayjs().subtract(1, 'month').startOf('month').toDate()
      endDate = dayjs().subtract(1, 'month').endOf('month').toDate()
    } else if (mode == 'ADVANCED' && startDateInput && endDateInput) {
      startDate = dayjs.unix(parseInt(startDateInput)).toDate()
      endDate = dayjs.unix(parseInt(endDateInput)).toDate()
      if (startDate > endDate) {
        throw new AppError('BAD_REQUEST', 'INVALID_PARAMS')
      }
    } else {
      throw new AppError('BAD_REQUEST', 'INVALID_PARAMS')
    }
    const orders_detail = await prisma.order_detail.findMany({
      where: {
        order: {
          created_at: { gte: startDate, lte: endDate }
        }
      },
      select: {
        price: true,
        order: {
          select: {
            created_at: true
          }
        }
      }
    })

    const totalAmountsMap = new Map<string, number>()

    orders_detail.forEach((item) => {
      const date = dayjs(item.order.created_at).format('YYYY-MM-DD')
      if (!totalAmountsMap.has(date)) {
        totalAmountsMap.set(date, 0)
      }
      totalAmountsMap.set(date, totalAmountsMap.get(date)! + item.price)
    })

    const totalAmounts: { date: string; totalAmount: number }[] = []

    totalAmountsMap.forEach((totalAmount, date) => {
      totalAmounts.push({ date, totalAmount })
    })
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', totalAmounts))
  } catch (error: any) {
    next(error)
  }
}

export const getBestSelling = async (req: Request, res: Response, next: NextFunction) => {
  const { mode, startDateInput, endDateInput } = req.query as IGetBestSelling
  try {
    let startDate
    let endDate

    if (mode === 'THIS_MONTH') {
      startDate = dayjs().startOf('month').toDate()
      endDate = dayjs().endOf('month').toDate()
    } else if (mode === 'THIS_WEEK') {
      startDate = dayjs().startOf('week').toDate()
      endDate = dayjs().endOf('week').toDate()
    } else if (mode === 'LAST_WEEK') {
      startDate = dayjs().subtract(1, 'week').startOf('week').toDate()
      endDate = dayjs().subtract(1, 'week').endOf('week').toDate()
    } else if (mode == 'LAST_MONTH') {
      startDate = dayjs().subtract(1, 'month').startOf('month').toDate()
      endDate = dayjs().subtract(1, 'month').endOf('month').toDate()
    } else if (mode == 'ADVANCED' && startDateInput && endDateInput) {
      startDate = dayjs.unix(parseInt(startDateInput)).toDate()
      endDate = dayjs.unix(parseInt(endDateInput)).toDate()
    } else {
      throw new AppError('BAD_REQUEST', 'INVALID_PARAMS')
    }

    const orders_detail = await prisma.order_detail.findMany({
      where: {
        order: {
          created_at: {
            gte: startDate,
            lte: endDate
          }
        }
      },
      select: {
        quantity: true,
        variant: {
          select: {
            product: {
              select: {
                id: true,
                name: true
              }
            }
          }
        }
      }
    })

    const productOrderQuantityMap = new Map<number, number>()
    orders_detail.forEach((orderDetail) => {
      const productId = orderDetail.variant.product.id
      const quantity = orderDetail.quantity
      if (!productOrderQuantityMap.has(productId)) {
        productOrderQuantityMap.set(productId, 0)
      }
      productOrderQuantityMap.set(productId, productOrderQuantityMap.get(productId)! + quantity)
    })

    const productOrderQuantityArray = Array.from(productOrderQuantityMap.entries())

    productOrderQuantityArray.sort((a, b) => b[1] - a[1])
    const topProducts = productOrderQuantityArray.slice(0, 10)

    const topProductsInfo = await Promise.all(
      topProducts.map(async ([productId, totalQuantity]) => {
        const productInfo = await prisma.product.findUnique({
          where: {
            id: productId
          },
          select: {
            id: true,
            name: true
          }
        })
        return { ...productInfo, totalQuantity }
      })
    )
    return res.status(200).json(resultTemplate('SUCCESS', 'SUCCESS', topProductsInfo))
  } catch (error: any) {
    next(error)
  }
}
