import prisma from '@/libs/prisma'
import { badWords } from 'vn-badwords'
import * as toxicity from '@tensorflow-models/toxicity'

export const updateRatingInfor = async (product_id: number) => {
  const new_rating_infor = await prisma.review.aggregate({
    where: {
      order_detail: {
        variant: {
          product_id: product_id
        }
      },
      is_disable: false
    },
    _avg: {
      rating: true
    }
  })
  // update the average rating of product
  if (new_rating_infor._avg.rating ) {
    await prisma.product.update({
      where: {
        id: product_id
      },
      data: {
        avg_rating: parseFloat(new_rating_infor._avg.rating.toFixed(2))
      }
    })
  }
}

export const validateContentReview = async (content: string) => {
  // const pattern = new RegExp(
  //   '^(https?:\\/\\/)?' + // protocol
  //     '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
  //     '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
  //     '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
  //     '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
  //     '(\\#[-a-z\\d_]*)?$',
  //   'i'
  // ) // fragment locator
  const pattern = /https?:\/\/\S+/
  let is_disable: boolean | string = false
  is_disable = !!pattern.test(content)
  if (is_disable) return is_disable
  is_disable = badWords(String(content), { validate: true })
  if (is_disable) return is_disable
  const threshold = 0.9
  const model = await toxicity.load(threshold, [
    'identity_attack',
    'insult',
    'obscene',
    'severe_toxicity',
    'sexual_explicit',
    'threat',
    'toxicity'
  ])
  const result = await model.classify(content)
  console.log('result:', result)

  for (let i = 0; i < result.length; i++) {
    const results = result[i].results

    for (let j = 0; j < results.length; j++) {
      if (results[j].match === true) {
        is_disable = true
        break
      }
    }
    if (is_disable) {
      break
    }
  }
  return is_disable
}
