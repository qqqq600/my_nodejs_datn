import { DeleteObjectCommand, PutObjectCommand, S3Client } from '@aws-sdk/client-s3'
import multer, { Multer } from 'multer'
import multerS3 from 'multer-s3'
import path from 'path'
import dotenv from 'dotenv'
import { AppError } from '@/config/appError'
import logger from '@/libs/logger'

dotenv.config()

export type ImageType = 'product' | 'review' | 'user' | 'banner' | 'category'

const AWS_S3_ACCOUNT_ID = process.env.AWS_S3_ACCOUNT_ID
const AWS_S3_ACCESS_KEY_ID = process.env.AWS_S3_ACCESS_KEY_ID
const AWS_S3_SECRET_ACCESS_KEY = process.env.AWS_S3_SECRET_ACCESS_KEY
const AWS_S3_BUCKET = process.env.AWS_S3_BUCKET
const AWS_S3_REGION = process.env.AWS_S3_REGION
if (!AWS_S3_ACCOUNT_ID || !AWS_S3_ACCESS_KEY_ID || !AWS_S3_SECRET_ACCESS_KEY || !AWS_S3_BUCKET || !AWS_S3_REGION) {
  throw new Error(
    'AWS_S3_ACCOUNT_ID or AWS_S3_ACCESS_KEY_ID or AWS_S3_SECRET_ACCESS_KEY or AWS_S3_BUCKET is not defined.'
  )
}

const s3 = new S3Client({
  credentials: {
    accessKeyId: AWS_S3_ACCESS_KEY_ID,
    secretAccessKey: AWS_S3_SECRET_ACCESS_KEY
  },
  region: AWS_S3_REGION
})

const s3Storage = (folder: ImageType) =>
  multerS3({
    s3: s3,
    bucket: AWS_S3_BUCKET,
    acl: 'public-read',
    metadata: (req, file, cb) => {
      cb(null, { fieldname: file.fieldname })
    },
    key: (req, file, cb) => {
      const dir = `${folder}/`
      const fileName = Date.now() + '_' + file.fieldname + '_' + file.originalname
      cb(null, dir + fileName)
    }
  })

function sanitizeFile(file: Express.Multer.File, cb: multer.FileFilterCallback) {
  // Define the allowed extension
  const fileExts = ['.png', '.jpg', '.jpeg', '.gif', '.webp']

  // Check allowed extensions
  const isAllowedExt = fileExts.includes(path.extname(file.originalname.toLowerCase()))

  // Mime type must be an image
  const isAllowedMimeType = file.mimetype.startsWith('image/')

  if (isAllowedExt && isAllowedMimeType) {
    return cb(null, true) // no errors
  } else {
    // pass error msg to callback, which can be displayed in frontend
    cb(new AppError('BAD_REQUEST', 'FILE_TYPE_NOT_ALLOWED'))
  }
}

export const uploadImage = (folder: ImageType, fileSizeMb?: number) =>
  multer({
    storage: s3Storage(folder),
    fileFilter: (req, file, callback) => {
      sanitizeFile(file, callback)
    },
    limits: {
      fileSize: fileSizeMb ? 1024 * 1024 * fileSizeMb : 1024 * 1024 * 1 // 1mb file size
    }
  })

export const deleteImage = async (imageUrl: string) => {
  // Tách URL để lấy key
  const urlParts = imageUrl.split('/')
  const bucket = urlParts[2].split('.')[0] // Lấy tên bucket từ URL
  const key = urlParts.slice(3).join('/') // Lấy key từ URL
  console.log(bucket, key)
  const deleteParams = {
    Bucket: bucket,
    Key: key
  }

  try {
    await s3.send(new DeleteObjectCommand(deleteParams))
    logger.info(`File ${key} deleted successfully`)
  } catch (err) {
    logger.error('Error', err)
  }
}
