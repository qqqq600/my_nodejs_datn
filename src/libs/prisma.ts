import { PrismaClient } from '@prisma/client'
import logger from './logger'

const DATABASE_URL = process.env.DATABASE_URL

if (!DATABASE_URL) {
  logger.error('DATABASE_URL is not defined.')
}
const prisma = new PrismaClient({
  datasources: {
    db: {
      url: DATABASE_URL
    }
  }
})

export default prisma
