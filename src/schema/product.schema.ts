import { Static, Type } from '@sinclair/typebox'
import { nameSchema, numberSchema } from './common.schema'

//Declare type
export const createProductSchema = Type.Object({
  name: nameSchema,
  manufacturer: Type.Optional(nameSchema),
  pattern: Type.Optional(nameSchema),
  material: Type.Optional(nameSchema),
  style: Type.Optional(nameSchema),
  others: Type.Optional(
    Type.Array(
      Type.Object({
        name: nameSchema,
        value: nameSchema
      })
    )
  ),
  category_id: Type.Number()
})

//Export Type
export type ICreateProduct = Static<typeof createProductSchema>

export const updateProductSchema = Type.Object({
  id: Type.Number(),
  name: Type.Optional(nameSchema),
  manufacturer: Type.Optional(nameSchema),
  pattern: Type.Optional(nameSchema),
  material: Type.Optional(nameSchema),
  style: Type.Optional(nameSchema),
  others: Type.Optional(
    Type.Array(
      Type.Object({
        name: nameSchema,
        value: nameSchema
      })
    )
  ),
  category_id: Type.Optional(Type.Number())
})

export type IUpdateProduct = Static<typeof updateProductSchema>

export const deleteProductSchema = Type.Object({
  id: numberSchema
})

export const getVariantSchema = Type.Object({
  product_id: Type.Optional(numberSchema),
  color_id: Type.Optional(numberSchema),
  size: Type.Optional(Type.String())
})

export type IGetVariant = Static<typeof getVariantSchema>

export const createVariantSchema = Type.Object({
  size: Type.Optional(Type.String()),
  price_import: Type.Optional(numberSchema),
  price_export: Type.Optional(numberSchema),
  price_sale: Type.Optional(numberSchema),
  remain_quantity: Type.Optional(numberSchema),
  color_id: numberSchema,
  product_id: numberSchema
})

export type ICreateVariant = Static<typeof createVariantSchema>

export const updateVariantSchema = Type.Object({
  id: Type.Number({ minimum: 1 }),
  size: Type.Optional(Type.String()),
  price_import: Type.Optional(Type.Number({ minimum: 1 })),
  price_export: Type.Optional(Type.Number({ minimum: 1 })),
  price_sale: Type.Optional(Type.Number({ minimum: 1 })),
  remain_quantity: Type.Optional(Type.Number({ minimum: 0 })),
  color_id: Type.Optional(Type.Number({ minimum: 1 })),
  product_id: Type.Optional(Type.Number({ minimum: 1 }))
})

export type IUpdateVariant = Static<typeof updateVariantSchema>

export const createVariantImageSchema = Type.Object({
  id: numberSchema
})
export type ICreateVariantImage = Static<typeof createVariantImageSchema>

export const deleteVariantImageSchema = Type.Object({
  id: numberSchema
})

export type IDeleteVariantImage = Static<typeof deleteVariantImageSchema>

export const getProductSchema = Type.Object({
  product_id: Type.Optional(Type.Number()),
  category_id: Type.Optional(Type.Number()),
  search: Type.Optional(Type.String()),
  rating: Type.Optional(Type.Number()),
  sort_by: Type.Optional(Type.String()),
  min_price: Type.Optional(Type.Number()),
  max_price: Type.Optional(Type.Number()),
  voucher_id: Type.Optional(Type.Number()),
  image: Type.Optional(Type.String()),
  page: Type.Optional(Type.Number()),
  limit: Type.Optional(Type.Number())
})
export type IGetProduct = Static<typeof getProductSchema>

export const getProductDetailSchema = Type.Object({
  product_id: nameSchema,
  star: Type.Optional(numberSchema)
})

export type IGetProductDetail = Static<typeof getProductDetailSchema>
